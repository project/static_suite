# Astro Static Builder
This module is a plugin for Static Build module and builds a static site using
[Astro](https://astro.build/).

## INTRODUCTION ##
It works by spawning a background process which actually runs a `astro`
command on a bash shell.

That process is run by the user running the web server (usually `www-data` or
similar) so you must ensure that user can run a
`node node_modules/.bin/astro build` command (that means having `node`
installed and available in that user's `$PATH` environment variable).

## REQUIREMENTS ##
It depends on Static Build module.

## INSTALLATION ##
Run `composer require drupal/static_builder_astro`.

Follow the instructions available at `/admin/config/static/build`, and
create the directory structure as stated in that configuration page.

You should now have a folder
`[BASE_DIRECTORY]/astro/[live|preview]/.build`.

Inside that folder, add Astro files and run `npm install` or `yarn install`
inside it, so everything required by Astro is in place.

As a best practice:
 * Ensure that running `node node_modules/.bin/astro build` inside `.build`
   folder works without errors before trying to use this module.
 * Change ownership of all files inside `.build` folder so they belong to
   `www-data` user. This usually makes executing Astro builds much faster.

## CONFIGURATION ##
There are two configuration types involved in this module.
* global configuration for the Static Build module:
  `/admin/config/static/build`
* Astro builder configuration: `/admin/config/static/build/astro`
