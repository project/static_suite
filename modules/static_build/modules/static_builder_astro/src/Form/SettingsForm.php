<?php

namespace Drupal\static_builder_astro\Form;

use Drupal\static_build\Form\LocalBuilderSettingsFormBase;

/**
 * Configuration form for Static Builder Astro.
 */
class SettingsForm extends LocalBuilderSettingsFormBase {

  /**
   * {@inheritdoc}
   */
  public function getBuilderId(): string {
    return 'astro';
  }

}
