<?php

namespace Drupal\static_builder_astro\Plugin\static_build\StaticBuilder;

use Drupal\static_build\Plugin\LocalStaticBuilderPluginBase;

/**
 * Provides a static builder for Astro.
 *
 * @StaticBuilder(
 *  id = "astro",
 *  label = @Translation("Astro"),
 *  description = @Translation("Static builder to build sites using Astro"),
 *  engine = "node",
 *  command = "node_modules/.bin/astro build",
 *  out_dir = "dist",
 *  cache_dir = null,
 *  host = "local"
 * )
 */
class AstroStaticBuilder extends LocalStaticBuilderPluginBase {

}
