<?php

namespace Drupal\static_builder_aws_lambda\Plugin\static_build\StaticBuilder;

use Aws\Credentials\Credentials;
use Aws\Lambda\LambdaClient;
use Drupal\Core\Site\Settings;
use Drupal\static_build\Plugin\StaticBuilderPluginBase;
use Drupal\static_suite\StaticSuiteException;

/**
 * Provides a static builder for AWS Lambda.
 *
 * @StaticBuilder(
 *  id = "aws_lambda",
 *  label = @Translation("AWS Lambda"),
 *  description = @Translation("Static builder that uses AWS Lambda"),
 *  host = "cloud"
 * )
 */
class AwsLambdaStaticBuilder extends StaticBuilderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function build(): void {
    $clientConfig = [
      'version' => 'latest',
      'region' => $this->configuration['region'],
    ];

    if (!$this->configuration['use_iam_credentials']) {
      $clientConfig['credentials'] = new Credentials(
        $this->configuration['access-key'],
        $this->configuration['secret-key']
      );
    }

    // Set up client.
    $lambdaClient = new LambdaClient($clientConfig);

    $this->logMessage('Annex build started.');
    $this->logMessage(
      'Please be patient, no data will be logged until lambda function finishes and returns its response.'
    );

    $result = $lambdaClient->invoke(
      [
        'FunctionName' => $this->configuration['function-name'],
        'InvocationType' => 'RequestResponse',
        'LogType' => 'Tail',
      ]
    );

    $logResult = base64_decode($result->get('LogResult'));
    $logResultLines = explode("\n", $logResult);
    foreach ($logResultLines as $logResultLine) {
      $this->logMessage(str_replace("\t", " ", $logResultLine));
    }
    if ($result->get('StatusCode') !== 200 || !empty(
      $result->get(
        'FunctionError'
      )
      )) {
      throw new StaticSuiteException($result->get('FunctionError') . "\n" . $logResult);
    }
    $this->logMessage('Build completed');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $config = $this->configFactory->get('static_builder_aws_lambda.settings');
    return parent::defaultConfiguration() + [
      'function-name' => $config->get('function_name'),
      'region' => $config->get('region'),
      'use_iam_credentials' => $config->get('use_iam_credentials'),
      'access-key' => Settings::get('static_builder_aws_lambda.access_key'),
      'secret-key' => Settings::get('static_builder_aws_lambda.secret_key'),
    ];
  }

}
