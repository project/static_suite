<?php

namespace Drupal\static_builder_gatsby\Form;

use Drupal\static_build\Form\LocalBuilderSettingsFormBase;

/**
 * Configuration form for Static Builder Gatsby.
 */
class SettingsForm extends LocalBuilderSettingsFormBase {

  /**
   * {@inheritdoc}
   */
  public function getBuilderId(): string {
    return 'gatsby';
  }

}
