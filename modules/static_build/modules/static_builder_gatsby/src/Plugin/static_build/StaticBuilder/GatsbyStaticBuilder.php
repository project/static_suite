<?php

namespace Drupal\static_builder_gatsby\Plugin\static_build\StaticBuilder;

use Drupal\static_build\Plugin\LocalStaticBuilderPluginBase;

/**
 * Provides a static builder for Gatsby.
 *
 * @StaticBuilder(
 *  id = "gatsby",
 *  label = @Translation("Gatsby"),
 *  description = @Translation("Static builder to generate sites using Gatsby"),
 *  engine = "node",
 *  command = "node_modules/.bin/gatsby build",
 *  out_dir = "public",
 *  cache_dir = ".cache",
 *  host = "local",
 * )
 */
class GatsbyStaticBuilder extends LocalStaticBuilderPluginBase {

}
