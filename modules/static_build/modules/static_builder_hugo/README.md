# Hugo Static Builder
This module is a plugin for Static Build module and builds a static site using
[Hugo](https://gohugo.io/).

## INTRODUCTION ##
It works by spawning a background process which actually runs a `hugo`
command on a bash shell.

That process is run by the user running the web server (usually `www-data` or
similar) so you must ensure that user can run a `hugo` command (that
means having `hugo` installed and available in that user's `$PATH`
environment variable).

## REQUIREMENTS ##
It depends on Static Build module.

## INSTALLATION ##
Run `composer require drupal/static_builder_hugo`.

Follow the instructions available at `/admin/config/static/build`, and
create the directory structure as stated in that configuration page.

You should now have a folder
`[BASE_DIRECTORY]/hugo/[live|preview]/.build`.

Inside that folder, add files required by Hugo to run its build.

As a best practice:
 * Ensure that running `hugo` inside `.build` folder works without
   errors before trying to use this module.
 * Change ownership of all files inside `.build` folder so they belong to
   `www-data` user. This usually makes executing Hugo builds much faster.

## CONFIGURATION ##
There are two configuration types involved in this module.
* global configuration for the Static Build module:
  `/admin/config/static/build`
* Hugo builder configuration: `/admin/config/static/build/hugo`
