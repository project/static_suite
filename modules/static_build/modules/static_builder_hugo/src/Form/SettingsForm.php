<?php

namespace Drupal\static_builder_hugo\Form;

use Drupal\static_build\Form\LocalBuilderSettingsFormBase;

/**
 * Configuration form for Static Builder Hugo.
 */
class SettingsForm extends LocalBuilderSettingsFormBase {

  /**
   * {@inheritdoc}
   */
  public function getBuilderId(): string {
    return 'hugo';
  }

}
