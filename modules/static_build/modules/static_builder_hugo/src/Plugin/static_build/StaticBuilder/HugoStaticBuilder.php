<?php

namespace Drupal\static_builder_hugo\Plugin\static_build\StaticBuilder;

use Drupal\static_build\Plugin\LocalStaticBuilderPluginBase;

/**
 * Provides a static builder for Hugo.
 *
 * @StaticBuilder(
 *  id = "hugo",
 *  label = @Translation("Hugo"),
 *  description = @Translation("Static builder to build sites using Hugo"),
 *  engine = null,
 *  command = "hugo",
 *  out_dir = "public",
 *  cache_dir = null,
 *  host = "local"
 * )
 */
class HugoStaticBuilder extends LocalStaticBuilderPluginBase {

}
