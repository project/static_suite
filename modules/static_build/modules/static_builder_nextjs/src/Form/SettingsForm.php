<?php

namespace Drupal\static_builder_nextjs\Form;

use Drupal\static_build\Form\LocalBuilderSettingsFormBase;

/**
 * Configuration form for Static Builder Next.js.
 */
class SettingsForm extends LocalBuilderSettingsFormBase {

  /**
   * {@inheritdoc}
   */
  public function getBuilderId(): string {
    return 'nextjs';
  }

}
