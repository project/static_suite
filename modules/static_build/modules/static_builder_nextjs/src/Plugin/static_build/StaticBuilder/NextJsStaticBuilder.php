<?php

namespace Drupal\static_builder_nextjs\Plugin\static_build\StaticBuilder;

use Drupal\static_build\Plugin\LocalStaticBuilderPluginBase;

/**
 * Provides a static builder for Next.js.
 *
 * @StaticBuilder(
 *  id = "nextjs",
 *  label = @Translation("Next.js"),
 *  description = @Translation("Static builder to build sites using Next.js"),
 *  engine = "node",
 *  command = "node_modules/.bin/next build && node_modules/.bin/next export",
 *  out_dir = "out",
 *  cache_dir = ".next",
 *  host = "local"
 * )
 */
class NextJsStaticBuilder extends LocalStaticBuilderPluginBase {

}
