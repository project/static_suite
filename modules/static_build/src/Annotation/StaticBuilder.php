<?php

namespace Drupal\static_build\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a Static Builder plugin item annotation object.
 *
 * @see \Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface
 * @see plugin_api
 *
 * @Annotation
 */
class StaticBuilder extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $description;

  /**
   * A flag to tell whether it needs a copy of the data directory to work.
   *
   * Most builders use a copy of the data directory, locally copied into their
   * .build directory, to properly execute a build. That is not the case with
   * builders based on Data Server, which use data from its dump or by directly
   * accessing its in-memory storage.
   *
   * Valid for both local and cloud builders.
   *
   * True if not defined.
   *
   * @var bool|null
   */
  public ?bool $needs_data_dir;

  /**
   * The engine that runs the build command (node, php, perl, etc).
   *
   * If no engine is required (i.e.- hugo) use a null value.
   *
   * Only required for local builders.
   *
   * @var string|null
   */
  public ?string $engine;

  /**
   * The command run by the engine, if any (e.g.- node_modules/.bin/*** build).
   *
   * If an engine is specified, the command is appended to the engine:
   * "node node_modules/.bin/*** build"
   *
   * Only required for local builders.
   *
   * @var string|null
   */
  public ?string $command;

  /**
   * The directory where the build process outputs its results (out, public...)
   *
   * Only required for local builders.
   *
   * @var string|null
   */
  public ?string $out_dir;

  /**
   * The directory where the build process saves its cache data, if any.
   *
   * Only required for local builders.
   *
   * @var string|null
   */
  public ?string $cache_dir;

  /**
   * Where is this plugin meant to be run: local or cloud.
   *
   * @var string
   */
  public string $host;

}
