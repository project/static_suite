<?php

namespace Drupal\static_build\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\static_build\Event\StaticBuildEvents;
use Drupal\static_build\Plugin\StaticBuilderPluginInterface;
use Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface;
use Drupal\static_export\Exporter\ExporterPluginInterface;
use Drupal\static_export\Exporter\Type\Locale\LocaleExporterPluginInterface;
use Drupal\static_export\Exporter\Type\Locale\LocaleExporterPluginManagerInterface;
use Drupal\static_suite\Event\DataEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a controller to execute a new build on demand.
 */
class BuilderController extends ControllerBase {

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;

  /**
   * Symfony Event Dispatcher Interface.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The static builder plugin manager.
   *
   * @var \Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface
   */
  protected StaticBuilderPluginManagerInterface $staticBuilderPluginManager;

  /**
   * Locale exporter manager.
   *
   * @var \Drupal\static_export\Exporter\Type\Locale\LocaleExporterPluginManagerInterface
   */
  protected LocaleExporterPluginManagerInterface $localeExporterManager;

  /**
   * Must abort message.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected TranslatableMarkup $mustAbortMessage;

  /**
   * BuilderController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language Manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher service.
   * @param \Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface $staticBuilderPluginManager
   *   The static builder plugin manager.
   * @param \Drupal\static_export\Exporter\Type\Locale\LocaleExporterPluginManagerInterface $localeExporterManager
   *   Locale exporter manager.
   */
  public function __construct(
    RequestStack $requestStack,
    LanguageManagerInterface $languageManager,
    EventDispatcherInterface $eventDispatcher,
    StaticBuilderPluginManagerInterface $staticBuilderPluginManager,
    LocaleExporterPluginManagerInterface $localeExporterManager,
  ) {
    $this->request = $requestStack->getCurrentRequest();
    $this->languageManager = $languageManager;
    $this->eventDispatcher = $eventDispatcher;
    $this->staticBuilderPluginManager = $staticBuilderPluginManager;
    $this->localeExporterManager = $localeExporterManager;
    $this->mustAbortMessage = $this->t('Something has prevented the build to start, please try again in a few minutes.');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('language_manager'),
      $container->get('event_dispatcher'),
      $container->get('plugin.manager.static_builder'),
      $container->get('plugin.manager.static_locale_exporter'),
    );
  }

  /**
   * Run a new build (export and build).
   *
   * @param string $builderId
   *   Build plugin id.
   * @param string $runMode
   *   Build mode: live or preview.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to previous page.
   */
  public function runBuild(string $builderId, string $runMode): RedirectResponse {
    try {
      $exporter = $this->exportLocale();
      $hasBuildBeenExecuted = $this->executeBuild($exporter, $builderId, $runMode);

      if ($hasBuildBeenExecuted) {
        $messageType = MessengerInterface::TYPE_STATUS;
        $message = $this->t('A new "@runMode" build for "@builderId"  has started.', [
          '@builderId' => $builderId,
          '@runMode' => $runMode,
        ]);
      }
      else {
        $messageType = MessengerInterface::TYPE_WARNING;
        $message = $this->mustAbortMessage;
      }
    }
    catch (\Exception $e) {
      $messageType = MessengerInterface::TYPE_ERROR;
      $message = $e->getMessage();
    }

    $this->messenger()->addMessage($message, $messageType);

    // Wait 2 seconds for the process to start to be redirected.
    sleep(2);
    return new RedirectResponse($this->request->query->get('destination'));
  }

  /**
   * Execute a new build.
   *
   * @param \Drupal\static_export\Exporter\ExporterPluginInterface $exporter
   *   The exporter.
   * @param string $builderId
   *   Build plugin id.
   * @param string $runMode
   *   Build mode: live or preview.
   *
   * @return bool
   *   Returns true if a new build has been executed, false otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\static_suite\StaticSuiteUserException
   */
  protected function executeBuild(ExporterPluginInterface $exporter, string $builderId, string $runMode): bool {

    // Early check for opting out of this process.
    $dataEvent = new DataEvent([
      'exporter' => $exporter,
      'run-mode' => $runMode,
    ]);
    $preflightEvent = $this->eventDispatcher->dispatch($dataEvent, StaticBuildEvents::CHAINED_STEP_PREFLIGHT);

    if (!$preflightEvent->getDataItem('must-abort')) {
      $plugin = $this->staticBuilderPluginManager->getInstance([
        'plugin_id' => $builderId,
        'configuration' => [
          'run-mode' => $runMode,
          'lock-mode' => StaticBuilderPluginInterface::LOCK_MODE_LIVE,
        ],
      ]);
      $plugin->init();
      return TRUE;
    }

    if($message = $dataEvent->getDataItem('must-abort-message')) {
      $this->mustAbortMessage = $this->t($message);
    }

    return FALSE;
  }

  /**
   * Exports locale data to be able to run a new build.
   *
   * @return \Drupal\static_export\Exporter\Type\Locale\LocaleExporterPluginInterface
   *   The exporter.
   *
   * @throws \Drupal\static_suite\StaticSuiteException
   * @throws \Drupal\static_suite\StaticSuiteUserException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function exportLocale(): LocaleExporterPluginInterface {
    $localeExporter = $this->localeExporterManager->getDefaultInstance();

    $execOptions = [
      'standalone' => TRUE,
      'log-to-file' => TRUE,
      'lock' => TRUE,
      'build' => FALSE,
    ];

    $localeExporter->setMustRequestBuild($execOptions['build']);
    $localeExporter->setIsForceWrite(TRUE);
    $localeExporter->export(
      ['langcode' => $this->languageManager->getDefaultLanguage()->getId()],
      $execOptions['standalone'],
      $execOptions['log-to-file'],
      $execOptions['lock']
    );

    return $localeExporter;
  }

}
