<?php

namespace Drupal\static_build\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\static_build\Plugin\StaticBuilderPluginInterface;
use Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for local Static Builders.
 */
abstract class LocalBuilderSettingsFormBase extends ConfigFormBase {

  /**
   * The static builder plugin manager.
   *
   * @var \Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface
   */
  protected StaticBuilderPluginManagerInterface $staticBuilderPluginManager;

  /**
   * Constructs a BuilderSettingsFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   * The typed config manager.
   * @param \Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface $static_builder_manager
   *   The static builder plugin manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, StaticBuilderPluginManagerInterface $static_builder_manager) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->staticBuilderPluginManager = $static_builder_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('plugin.manager.static_builder'),
    );
  }

  /**
   * Returns the id of the builder being configured by this form.
   *
   * @return string
   *   The id of the builder.
   */
  abstract public function getBuilderId(): string;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'static_builder_' . $this->getBuilderId() . '_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['static_builder_' . $this->getBuilderId() . '.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $builderId = $this->getBuilderId();
    $builderDefinition = $this->staticBuilderPluginManager->getDefinition($builderId);
    $builderLabel = $builderDefinition['label'] ?? NULL;
    $builderEngine = $builderDefinition['engine'] ?? NULL;
    $builderOutDir = $builderDefinition['out_dir'] ?? NULL;
    $builderCacheDir = $builderDefinition['cache_dir'] ?? NULL;

    $config = $this->config('static_builder_' . $builderId . '.settings');

    $form['exec_dir'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Execution directory'),
      '#description' => $this->t('Enter the directory inside "[BASE_DIRECTORY]/:builderId/[live|preview]/.build" where :builderName should be executed (see "Directory structure for local builders" inside Static Build <a href="@url">settings page</a> for more information). Keep it empty unless using Yarn Workspaces, Lerna or similar. In such cases, a possible value could be "/packages/:builderId". It must start with a leading slash.', [
        ':builderId' => $builderId,
        ':builderName' => $builderLabel,
        '@url' => Url::fromRoute('static_build.settings')
          ->toString(),
      ]),
      '#default_value' => $config->get('exec_dir'),
    ];

    $runModes = [
      StaticBuilderPluginInterface::RUN_MODE_LIVE,
      StaticBuilderPluginInterface::RUN_MODE_PREVIEW,
    ];

    // An execution command is composed by two items:
    // 1) The *optional* engine item (node, php, perl, etc)
    // 2) The command item ("node node_modules/-bin/gatsby build", hugo,
    // myPhpScript.php, etc).
    // Here we conditionally define one or two items, depending on the value of
    // the "engine" key in the StaticBuilder annotation.
    $execItems = [];
    if ($builderEngine) {
      $execItems['engine'] = $builderEngine;
    }
    $execItems['command'] = $builderLabel;

    foreach ($runModes as $runMode) {
      $runModeLabel = ucfirst($runMode);
      $form[$runMode] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $this->t(':runModeLabel mode', [':runModeLabel' => $runModeLabel]),
        '#description' => $this->t('Options to use when building a ":runMode" release.', [':runMode' => $runMode]),
      ];

      foreach ($execItems as $execItem => $execLabel) {
        $form[$runMode][$execItem . '_' . $runMode . '_options'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Options for %execLabel process (:runMode mode)', [
            '%execLabel' => $execLabel,
            ':runMode' => $runMode,
          ]),
          '#description' => $this->t('Options passed to %execLabel process when building a ":runMode" release.', [
            '%execLabel' => $execLabel,
            ':runMode' => $runMode,
          ]),
          '#required' => FALSE,
          '#default_value' => $config->get($execItem . '.' . $runMode . '.options'),
        ];
      }
    }

    $form['advanced'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Advanced'),
    ];

    $form['advanced']['delete_out_dir'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete %out_dir folder before and after each build (not recommended)', ['%out_dir' => $builderOutDir]),
      '#description' => $this->t('Useful to maintain disk space under control.'),
      '#default_value' => $config->get('delete.out_dir'),
    ];

    if ($builderCacheDir) {
      $form['advanced']['delete_cache_dir'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Delete %cache_dir folder before and after each build (not recommended)', ['%cache_dir' => $builderCacheDir]),
        '#description' => $this->t('Useful if you experience issues due to stale cached data.'),
        '#default_value' => $config->get('delete.cache_dir'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $execDir = $form_state->getValue('exec_dir');
    if (!empty($execDir) && !str_starts_with($execDir, '/')) {
      $form_state->setErrorByName(
        'exec_dir',
        $this->t('Execution directory must start with a leading slash.'));
    }

    if (!empty($execDir) && str_contains($execDir, '..')) {
      $form_state->setErrorByName(
        'exec_dir',
        $this->t('Execution directory contains illegal characters.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $builderId = $this->getBuilderId();
    $builderDefinition = $this->staticBuilderPluginManager->getDefinition($builderId);

    $config = $this->config('static_builder_' . $builderId . '.settings');
    $config->set('exec_dir', $form_state->getValue('exec_dir'));
    if (!empty($builderDefinition['engine'])) {
      $config->set('engine.live.options', $form_state->getValue('engine_live_options'));
      $config->set('engine.preview.options', $form_state->getValue('engine_preview_options'));
    }
    $config->set('command.live.options', $form_state->getValue('command_live_options'));
    $config->set('command.preview.options', $form_state->getValue('command_preview_options'));
    if (!empty($builderDefinition['cache_dir'])) {
      $config->set('delete.cache_dir', $form_state->getValue('delete_cache_dir'));
    }
    $config->set('delete.out_dir', $form_state->getValue('delete_out_dir'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
