<?php

namespace Drupal\static_build\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface;
use Drupal\static_suite\Utility\SettingsUrlResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines menu items for all available builders.
 */
class BuilderMenuItems extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The static builder manager.
   *
   * @var \Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface
   */
  protected StaticBuilderPluginManagerInterface $staticBuilderPluginManager;

  /**
   * The settings URL resolver.
   *
   * @var \Drupal\static_suite\Utility\SettingsUrlResolverInterface
   */
  protected SettingsUrlResolverInterface $settingsUrlResolver;

  /**
   * Constructor.
   *
   * @param \Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface $staticBuilderPluginManager
   *   The static builder manager.
   * @param \Drupal\static_suite\Utility\SettingsUrlResolverInterface $settingsUrlResolver
   *   The settings URL resolver.
   */
  public function __construct(
    StaticBuilderPluginManagerInterface $staticBuilderPluginManager,
    SettingsUrlResolverInterface $settingsUrlResolver
  ) {
    $this->staticBuilderPluginManager = $staticBuilderPluginManager;
    $this->settingsUrlResolver = $settingsUrlResolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('plugin.manager.static_builder'),
      $container->get('static_suite.settings_url_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives[$base_plugin_definition['id']] = array_merge(
      $base_plugin_definition,
      [
        'parent' => 'static_suite.settings',
        'description' => 'Manage Static Build settings',
        'route_name' => 'static_build.settings',
      ]
    );

    $definitions = $this->staticBuilderPluginManager->getDefinitions();
    if (is_array($definitions) && count($definitions) > 0) {
      foreach ($definitions as $definition) {
        $configUrl = $this->settingsUrlResolver->setModule($definition['provider'])
          ->resolve();
        if ($configUrl) {
          $this->derivatives[$base_plugin_definition['id'] . '.' . $definition['id']] = array_merge(
            $base_plugin_definition,
            [
              'title' => $definition['label'],
              'route_name' => 'static_builder_' . $definition['id'] . '.settings',
              'parent' => $base_plugin_definition['id'] . ':' . $base_plugin_definition['id'],
            ]
          );
        }
      }
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
