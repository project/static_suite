<?php

namespace Drupal\static_build\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines menu items for release list.
 */
class ReleaseListMenuItems extends DeriverBase implements
    ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected RouteProviderInterface $routeProvider;

  /**
   * The static builder manager.
   *
   * @var \Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface
   */
  protected StaticBuilderPluginManagerInterface $staticBuilderPluginManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Routing\RouteProviderInterface $routeProvider
   *   The route provider.
   * @param \Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface $staticBuilderPluginManager
   *   The static builder manager.
   */
  public function __construct(
    RouteProviderInterface $routeProvider,
    StaticBuilderPluginManagerInterface $staticBuilderPluginManager
  ) {
    $this->routeProvider = $routeProvider;
    $this->staticBuilderPluginManager = $staticBuilderPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    $base_plugin_id
  ) {
    return new static(
      $container->get('router.route_provider'),
      $container->get('plugin.manager.static_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    if ($base_plugin_definition['id'] === 'static_build.release_list.preview') {
      $mode = 'preview';
      $definitions = $this->staticBuilderPluginManager->getLocalDefinitions();
    }
    else {
      $mode = 'live';
      $definitions = $this->staticBuilderPluginManager->getDefinitions();
    }

    if (is_array($definitions) && count($definitions) > 0) {
      $routeName = $base_plugin_definition['id'] . '.default';
      if (count($this->routeProvider->getRoutesByNames([$routeName])) === 1) {
        $this->derivatives[$base_plugin_definition['id']] = array_merge(
          $base_plugin_definition,
          [
            'parent' => 'static_build.reports.main',
            'description' => 'List of available ' . strtoupper(
                $mode
            ) . ' releases',
            'route_name' => $routeName,
          ]
        );
      }
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
