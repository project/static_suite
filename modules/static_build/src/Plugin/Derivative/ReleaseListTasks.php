<?php

namespace Drupal\static_build\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\static_build\Plugin\StaticBuilderPluginInterface;
use Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines release list tasks.
 */
class ReleaseListTasks extends DeriverBase implements
    ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected RouteProviderInterface $routeProvider;

  /**
   * The static builder manager.
   *
   * @var \Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface
   */
  protected StaticBuilderPluginManagerInterface $staticBuilderPluginManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Routing\RouteProviderInterface $routeProvider
   *   The route provider.
   * @param \Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface $staticBuilderPluginManager
   *   The static builder manager.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    RouteProviderInterface $routeProvider,
    StaticBuilderPluginManagerInterface $staticBuilderPluginManager
  ) {
    $this->configFactory = $configFactory;
    $this->routeProvider = $routeProvider;
    $this->staticBuilderPluginManager = $staticBuilderPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    $base_plugin_id
  ) {
    return new static(
      $container->get('config.factory'),
      $container->get('router.route_provider'),
      $container->get('plugin.manager.static_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    if ($base_plugin_definition['id'] === 'static_build.release_list.preview') {
      $definitions = $this->staticBuilderPluginManager->getLocalDefinitions();
    }
    else {
      $definitions = $this->staticBuilderPluginManager->getDefinitions();
    }

    $staticBuildSettings = $this->configFactory->get('static_build.settings')
      ->getRawData();

    if (is_array($definitions) && count($definitions) > 0) {
      $routeName = $base_plugin_definition['id'] . '.default';
      if (count($this->routeProvider->getRoutesByNames([$routeName])) === 1) {
        // Define first level (tab).
        $this->derivatives[$base_plugin_definition['id']] = array_merge(
          $base_plugin_definition,
          [
            'route_name' => $routeName,
            'base_route' => 'static_build.admin_reports',
          ]
        );

        // Define second level tasks.
        $i = 0;
        foreach ($definitions as $definition) {
          $routeName = $base_plugin_definition['id'];
          $routeParameters = ['builderId' => $definition['id']];
          $runMode = $this->getRunMode($routeName);

          // Show only the active builders.
          $activeBuilders = $staticBuildSettings[$runMode]['builders'];
          if (!in_array($definition['id'], $activeBuilders, TRUE)) {
            continue;
          }

          if ($i === 0) {
            $routeName .= '.default';
            $routeParameters = [];
          }
          $this->derivatives[$base_plugin_definition['id'] . '.' . $definition['id']] = array_merge(
            $base_plugin_definition,
            [
              'title' => $definition['label'],
              'route_name' => $routeName,
              'route_parameters' => $routeParameters,
              'parent_id' => $base_plugin_definition['id'] . ':' . $base_plugin_definition['id'],
            ]
          );
          $i++;
        }
      }
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

  /**
   * Get the run mode of route.
   *
   * @param string $routeName
   *   The actual route.
   *
   * @return string
   *   The runMode of the actual route.
   */
  protected function getRunMode(string $routeName): string {
    if (str_contains($routeName, StaticBuilderPluginInterface::RUN_MODE_LIVE)) {
      return StaticBuilderPluginInterface::RUN_MODE_LIVE;
    }
    return StaticBuilderPluginInterface::RUN_MODE_PREVIEW;
  }

}
