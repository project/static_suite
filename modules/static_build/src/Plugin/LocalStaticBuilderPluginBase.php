<?php

namespace Drupal\static_build\Plugin;

use Drupal\static_suite\StaticSuiteException;

/**
 * Base class for local Static Builder plugins.
 */
abstract class LocalStaticBuilderPluginBase extends StaticBuilderPluginBase {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\static_suite\StaticSuiteException
   */
  public function preBuild(): void {
    $this->deleteTempDirs();
  }

  /**
   * Get the build command to be executed.
   *
   * @return string
   *   The build command to be executed.
   */
  protected function getBuildCmd(): string {
    $config = $this->configFactory->get('static_builder_' . $this->pluginId . '.settings');

    $buildCmdParts = [];

    // An execution command is composed by two items:
    // 1) The *optional* engine item (node, php, perl, etc)
    // 2) The command item ("node node_modules/-bin/gatsby build", hugo,
    // myPhpScript.php, etc).
    // Here we conditionally define one or two items, depending on the value of
    // the "engine" key in the StaticBuilder annotation.
    $execItems = [];
    if (!empty($this->pluginDefinition['engine'])) {
      $execItems[] = 'engine';
    }
    $execItems[] = 'command';

    foreach ($execItems as $execItem) {
      $buildCmdParts[] = $this->pluginDefinition[$execItem];
      $options = trim($config->get($execItem . '.' . $this->configuration['run-mode'] . '.options'));
      // Check $execItem contains something before passing it to
      // escapeshellarg. Otherwise, escapeshellarg will output an empty string
      // surrounded by single quote marks ('').
      if ($options) {
        $buildCmdParts[] = escapeshellarg($options);
      }
    }

    return implode(' ', $buildCmdParts);
  }

  /**
   * Get the CWD where the build command is executed.
   *
   * @return string
   *   The CWD where the build command is executed.
   */
  public function getBuildCwd(): string {
    $config = $this->configFactory->get('static_builder_' . $this->pluginId . '.settings');
    return $this->configuration['build-dir'] . $config->get('exec_dir');
  }

  /**
   * Get the environment for the build command.
   *
   * @return array
   *   The environment for the build command.
   */
  public function getBuildEnv(): array {
    // Set "***_IS_PREVIEW" or "***_IS_LIVE" accordingly.
    $envName = strtoupper($this->pluginId . '_IS_' . $this->configuration['run-mode']);
    return [$envName => 'true'];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): void {
    $cwd = $this->getBuildCwd();
    if (!is_dir($cwd) || !is_readable($cwd)) {
      throw new StaticSuiteException("Unable to build " . $this->configuration['run-mode'] . ". CWD directory for " . $this->pluginId . " (" . $cwd . ") not present or not readable.");
    }

    $lines = [];
    $cliCommand = $this->cliCommandFactory->create($this->getBuildCmd(), $cwd, $this->getBuildEnv());
    $cliCommandRunningProcess = $cliCommand->open();
    $logStamp = strtoupper($this->pluginId);
    while ($line = $cliCommandRunningProcess->readStdOut()) {
      if ($line !== "\n") {
        $this->logMessage('[' . $logStamp . '] ' . trim($line));
        $lines[] = $line;
      }
    }

    // There are some builders that do not return a non-zero code when they
    // fail. Some others output warning data into $stdErr, instead of using
    // $stdErr only for errors. Just in case, output $stdErr to be able to
    // detect such edge cases.
    $stdErr = $cliCommandRunningProcess->getStdErrContents();
    if (!empty($stdErr)) {
      $this->logMessage('[' . $logStamp . '] ' . trim($stdErr));
      $lines[] = $stdErr;
    }

    $exitCode = $cliCommand->close();
    if ($exitCode !== 0) {
      throw new StaticSuiteException("Unable to build " . $this->configuration['run-mode'] . ". Exit code $exitCode. Trace: " . implode("\n", $lines) . '. Error output (stdErr): ' . $stdErr);
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\static_suite\StaticSuiteException
   */
  public function postBuild(): void {
    $config = $this->configFactory->get('static_builder_' . $this->pluginId . '.settings');

    if (empty($this->pluginDefinition['out_dir'])) {
      throw new StaticSuiteException('Required "out_dir" value in not defined in "' . $this->pluginId . '" plugin');
    }

    // Move or copy out dir contents to release dir.
    $outDir = $this->configuration['build-dir'] . $config->get('exec_dir') . "/" . $this->pluginDefinition['out_dir'];
    if ($config->get('delete.out_dir')) {
      $this->release->moveToDir($outDir);
    }
    else {
      $this->release->copyToDir($outDir);
    }

    $this->deleteTempDirs();
  }

  /**
   * Deletes cache_dir and out_dir.
   *
   * @throws \Drupal\static_suite\StaticSuiteException
   */
  public function deleteTempDirs(): void {
    $config = $this->configFactory->get('static_builder_' . $this->pluginId . '.settings');

    if (!empty($this->pluginDefinition['cache_dir']) && $config->get('delete.cache_dir')) {
      $this->deleteInsideBuildDir(($config->get('exec_dir') ? $config->get('exec_dir') . '/' : NULL) . $this->pluginDefinition['cache_dir']);
    }
    if (!empty($this->pluginDefinition['out_dir']) && $config->get('delete.out_dir')) {
      $this->deleteInsideBuildDir(($config->get('exec_dir') ? $config->get('exec_dir') . '/' : NULL) . $this->pluginDefinition['out_dir']);
    }
  }

}
