<?php

namespace Drupal\static_build\Routing;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\static_build\Plugin\StaticBuilderPluginInterface;
use Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Static Builder Manager.
   *
   * @var \Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface
   */
  protected StaticBuilderPluginManagerInterface $staticBuilderPluginManager;

  /**
   * Constructs the subscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config Factory.
   * @param \Drupal\static_build\Plugin\StaticBuilderPluginManagerInterface $staticBuilderPluginManager
   *   Static Builder Manager.
   */
  public function __construct(ConfigFactoryInterface $configFactory, StaticBuilderPluginManagerInterface $staticBuilderPluginManager) {
    $this->configFactory = $configFactory;
    $this->staticBuilderPluginManager = $staticBuilderPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    $this->createReleaseListRoutes($collection);
    $this->createSettingFormRoutes($collection);
  }

  /**
   * Create routes for the list of releases.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   The route collection for adding routes.
   */
  protected function createReleaseListRoutes(RouteCollection $collection) {
    $staticBuildSettings = $this->configFactory->get('static_build.settings')
      ->getRawData();

    $runModes = [
      StaticBuilderPluginInterface::RUN_MODE_LIVE,
      StaticBuilderPluginInterface::RUN_MODE_PREVIEW,
    ];
    foreach ($runModes as $runMode) {
      if ($runMode === StaticBuilderPluginInterface::RUN_MODE_LIVE) {
        $builderDefinitions = $this->staticBuilderPluginManager->getDefinitions();
      }
      else {
        $builderDefinitions = $this->staticBuilderPluginManager->getLocalDefinitions();
      }

      // Filter active builders.
      $activeBuilders = $staticBuildSettings[$runMode]['builders'];
      $builderDefinitions = array_filter($builderDefinitions, static function ($builderDefinition) use ($activeBuilders) {
        return in_array($builderDefinition, $activeBuilders, TRUE);
      }, ARRAY_FILTER_USE_KEY);

      if (is_array($builderDefinitions) && count($builderDefinitions) > 0) {
        // Add a default route for the first builder.
        $route = new Route(
          '/admin/reports/static/build/' . $runMode . '/releases/list',
          [
            '_controller' => '\Drupal\static_build\Controller\ReleaseController::listReleases',
            '_title' => 'Static Build - ' . Unicode::ucfirst($runMode) . ' releases',
            'runMode' => $runMode,
            'builderId' => array_keys($builderDefinitions)[0],
          ],
          ['_permission' => 'access site reports']
        );
        $collection->add('static_build.release_list.' . $runMode . '.default', $route);

        // Add another route for all builders.
        $route = new Route(
          '/admin/reports/static/build/' . $runMode . '/releases/list/{builderId}',
          [
            '_controller' => '\Drupal\static_build\Controller\ReleaseController::listReleases',
            '_title' => 'Static Build - ' . Unicode::ucfirst($runMode) . ' releases',
            'runMode' => $runMode,
          ],
          ['_permission' => 'access site reports']
        );
        $collection->add('static_build.release_list.' . $runMode, $route);
      }
    }
  }

  /**
   * Create routes for the settings form of builders.
   *
   * @param \Symfony\Component\Routing\RouteCollection $collection
   *   The route collection for adding routes.
   */
  protected function createSettingFormRoutes(RouteCollection $collection) {
    $builderDefinitions = $this->staticBuilderPluginManager->getDefinitions();
    if (is_array($builderDefinitions) && count($builderDefinitions) > 0) {
      foreach ($builderDefinitions as $builderDefinition) {
        $route = new Route(
          '/admin/config/static/build/' . $builderDefinition['id'],
          [
            '_form' => '\Drupal\static_builder_' . $builderDefinition['id'] . '\Form\SettingsForm',
            '_title' => 'Static Builder - ' . $builderDefinition['label'] . ': Settings',
          ],
          ['_permission' => 'administer site configuration']
        );
        $collection->add('static_builder_' . $builderDefinition['id'] . '.settings', $route);
      }
    }
  }

}
