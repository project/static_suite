<?php

namespace Drupal\static_deploy\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\static_build\Event\StaticBuildEvents;
use Drupal\static_deploy\Event\StaticDeployEvents;
use Drupal\static_deploy\Plugin\StaticDeployerPluginManagerInterface;
use Drupal\static_suite\Entity\EntityUtils;
use Drupal\static_suite\Event\DataEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for Static Deploy.
 */
class EventSubscriber implements EventSubscriberInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Static Deployer Manager.
   *
   * @var \Drupal\static_deploy\Plugin\StaticDeployerPluginManagerInterface
   */
  protected StaticDeployerPluginManagerInterface $staticDeployerPluginManager;

  /**
   * Entity Utils.
   *
   * @var \Drupal\static_suite\Entity\EntityUtils
   */
  protected EntityUtils $entityUtils;

  /**
   * Symfony Event Dispatcher Interface.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Constructs the subscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\static_deploy\Plugin\StaticDeployerPluginManagerInterface $staticDeployerPluginManager
   *   Static Deployer Manager.
   * @param \Drupal\static_suite\Entity\EntityUtils $entityUtils
   *   Utils for working with entities.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StaticDeployerPluginManagerInterface $staticDeployerPluginManager, EntityUtils $entityUtils, EventDispatcherInterface $event_dispatcher) {
    $this->configFactory = $config_factory;
    $this->staticDeployerPluginManager = $staticDeployerPluginManager;
    $this->entityUtils = $entityUtils;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[StaticBuildEvents::CHAINED_STEP_END][] = ['requestDeploy'];
    return $events;
  }

  /**
   * Reacts to a StaticBuildEvents::ENDS event.
   *
   * @param \Drupal\static_suite\Event\DataEvent $event
   *   The Static Build event.
   *
   * @return \Drupal\static_suite\Event\DataEvent
   *   The processed event.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\static_suite\StaticSuiteException
   */
  public function requestDeploy(DataEvent $event): DataEvent {
    // Check if received event is requesting a deployment.
    $builder = $event->getDataItem('builder');
    if ($builder) {
      // Early check for opting out of this process.
      $dataEvent = new DataEvent(['builder' => $builder]);
      $preflightEvent = $this->eventDispatcher->dispatch($dataEvent, StaticDeployEvents::CHAINED_STEP_PREFLIGHT);
      if ($preflightEvent->getDataItem('must-abort')) {
        $builder->logMessage('[deploy] Abort signal received, deploy will not be triggered.');
        return $event;
      }

      $configuration = $builder->getConfiguration();
      if (!$configuration['request-deploy']) {
        $builder->logMessage('[deploy] Builder is not allowed to trigger a deployment.');
        return $event;
      }

      $deployers = $this->configFactory->get('static_deploy.settings')
        ->get('deployers');
      if (!is_array($deployers) || count($deployers) === 0) {
        $builder->logMessage('[deploy] No deployers available.');
        return $event;
      }

      foreach ($deployers as $deployerId) {
        $staticDeployer = $this->staticDeployerPluginManager->getInstance([
          'plugin_id' => $deployerId,
          'configuration' => [
            'builder-id' => $builder->getPluginId(),
            'console-output' => $configuration['console-output'] ?? NULL,
          ],
        ]);

        // Trigger a deployment.
        $builder->logMessage('[deploy] Deployment triggered.');
        $staticDeployer->init();
      }
    }

    return $event;
  }

}
