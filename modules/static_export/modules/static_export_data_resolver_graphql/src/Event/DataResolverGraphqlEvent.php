<?php

namespace Drupal\static_export_data_resolver_graphql\Event;

use Drupal\static_export\Entity\ExportableEntity;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event for data resolver graphql.
 */
class DataResolverGraphqlEvent extends Event {

  /**
   * Constructs the object.
   *
   * @param string $graphqlFilePath
   *   The graphql file path.
   * @param \Drupal\static_export\Entity\ExportableEntity $exportableEntity
   *   The exportable entity.
   * @param string|null $variant
   *   Variant key.
   */
  public function __construct(protected string $graphqlFilePath, protected ExportableEntity $exportableEntity, protected ?string $variant) {
  }

  /**
   * Get the exportable entity.
   *
   * @return \Drupal\static_export\Entity\ExportableEntity
   *   The exportable entity
   */
  public function getExportableEntity(): ExportableEntity {
    return $this->exportableEntity;
  }

  /**
   * Get the variant key.
   *
   * @return string|null
   *   The variant key.
   */
  public function getVariant(): ?string {
    return $this->variant;
  }

  /**
   * Set the graphql file path.
   *
   * @param string $path
   *   The graphql file path.
   */
  public function setGraphqlFile(string $path): void {
    $this->graphqlFilePath = $path;
  }

  /**
   * Get the graphql file path.
   *
   * @return string
   *   The graphql file path.
   */
  public function getGraphqlFile(): string {
    return $this->graphqlFilePath;
  }

}
