<?php

namespace Drupal\static_export_data_resolver_graphql\Event;

/**
 * Contains all events dispatched by Data Resolver Graphql.
 */
final class DataResolverGraphqlEvents {

  public const CALCULATE_GRAPHQL_FILE_PATH = 'data_resolver_graphql.calculate_graphql_file_path';

}
