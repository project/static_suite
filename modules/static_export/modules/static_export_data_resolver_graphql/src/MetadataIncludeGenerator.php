<?php

namespace Drupal\static_export_data_resolver_graphql;

/**
 * Metadata Include Generator.
 *
 * Adds to result all includes (custom, locale, entity, config and query)
 *  path into metadata.includes.
 */
class MetadataIncludeGenerator implements MetadataIncludeGeneratorInterface {

  protected const REGEXP_PATTERN = '/(entity|config|locale|custom|query)Include$/i';

  protected const STATIC_REGEXP_PATTERN = '/(entity|config|locale|custom)Include$/i';

  protected const DYNAMIC_REGEXP_PATTERN = '/queryInclude$/i';

  /**
   * {@inheritdoc}
   */
  public function addIncludeMetas(array $data): array {
    $includes = $this->getIncludes($data);
    if (count($includes) > 0) {
      foreach ($includes as $includePath => $includeValue) {
        if (preg_match(self::STATIC_REGEXP_PATTERN, $includePath) === 1) {
          $data['metadata']['includes']['static'][$includePath] = $includeValue;
        }
        elseif (preg_match(self::DYNAMIC_REGEXP_PATTERN, $includePath) === 1) {
          $data['metadata']['includes']['dynamic'][$includePath] = $includeValue;
        }
      }
    }
    return $data;
  }

  /**
   * Get includes.
   *
   * @param array $tree
   *   Array part to be parsed.
   * @param string|null $parentPath
   *   Parent path.
   *
   * @return array
   *   Array with include paths in the array part
   */
  protected function getIncludes(array $tree, ?string $parentPath = NULL): array {
    $paths = [];
    if ($parentPath !== NULL) {
      $parentPath .= '.';
    }
    foreach ($tree as $k => $v) {
      if (is_array($v)) {
        $currentPath = $parentPath . $k;
        $paths = array_merge($paths, $this->getIncludes($v, $currentPath));
      }
      elseif (preg_match(self::REGEXP_PATTERN, $k) === 1) {
        $paths[$parentPath . $k] = $v;
      }
    }
    return $paths;
  }

}
