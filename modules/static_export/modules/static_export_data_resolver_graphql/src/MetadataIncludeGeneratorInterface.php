<?php

namespace Drupal\static_export_data_resolver_graphql;

/**
 * An Interface for Metadata Include Generator.
 */
interface MetadataIncludeGeneratorInterface {

  /**
   * Add include metadata.
   *
   * @param array $data
   *   Data as a multidimensional array.
   *
   * @return array
   *   Array with metadata of includes added if they exist.
   */
  public function addIncludeMetas(array $data): array;

}
