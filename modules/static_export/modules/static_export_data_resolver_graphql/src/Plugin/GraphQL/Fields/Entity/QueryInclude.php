<?php

namespace Drupal\static_export_data_resolver_graphql\Plugin\GraphQL\Fields\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Query Include.
 *
 * @GraphQLField(
 *   id = "query_include",
 *   secure = true,
 *   name = "queryInclude",
 *   description = "Includes dynamic data coming from a query executed by the
 *   Data Server. It takes two arguments: 1) id: query identifier to be
 *   executed; 2) args: query string to be passed to the Data Server (e.g.-
 *   limit=10&order=desc). Three tokes are also available for 'args' when this
 *   field's value is an Entity: {entity_type}, {bundle} and {id}. (e.g.-
 *   id={id}&bundle={bundle})", type = "String", parents= {
 *    "Root",
 *    "Language",
 *    "Url",
 *    "EntityQueryResult",
 *    "Entity",
 *    "Section",
 *    "LayoutSetting",
 *    "Component",
 *    "Metatag",
 *    "ImageResource",
 *    "Menu",
 *    "MenuLink",
 *    "Link"
 *   },
 *   arguments = {
 *     "id" = "String!",
 *     "args" = "String",
 *   },
 *   nullable = true
 * )
 *
 * INFO: All parents types =
 *   {"Root","Language","Url","EntityQueryResult","Entity","Section","LayoutSetting","Component","Metatag","ImageResource","Menu","MenuLink","Link","InternalResponse","LanguageSwitchLink","EntityDescribable","EntityBrowser","EntityCrudOutput","ConstraintViolation","ExternalResponse"}
 */
class QueryInclude extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configuration = $configuration;
    $this->pluginId = $plugin_id;
    $this->pluginDefinition = $plugin_definition;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Helper function to tll whether an entity is a Paragraph.
   *
   * To avoid adding a dependency to the Paragraphs module, it checks the names
   * of its interfaces, instead of using "instanceof".
   *
   * @param \Drupal\Core\Entity\EntityInterface $value
   *   The entity to be checked.
   *
   * @return bool
   *   True if it is a Paragraph, false otherwise.
   */
  protected static function isParagraph(EntityInterface $value): bool {
    return in_array("Drupal\paragraphs\ParagraphInterface", class_implements($value), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof EntityInterface && isset($args['args'])) {
      // Parse standard arguments.
      $args['args'] = str_replace(
        ['{id}', '{uuid}', '{bundle}', '{entity_type}', '{langcode}'],
        [
          $value->id(),
          $value->uuid(),
          $value->bundle(),
          $value->getEntityTypeId(),
          $context->getContext('language', $info),
        ],
        $args['args']
      );

      if (str_contains($args['args'], '{')) {
        parse_str($args['args'], $queryArray);
        foreach ($queryArray as $key => $arg) {
          if (preg_match("/^\{.*\}$/", $arg)) {
            $field = str_replace(['{', '}'], '', $arg);
            // Parse field arguments.
            if ($value instanceof FieldableEntityInterface && $value->hasField($field)) {
              $queryArray[$key] = $value->get($field)->getString();
            }
            // Parse paragraph arguments.
            if (str_starts_with($arg, '{paragraph_top_parent:') && self::isParagraph($value)) {
              $parent = $value->getParentEntity();
              while ($parent && self::isParagraph($parent)) {
                $parent = $parent->getParentEntity();
              }
              $parentField = str_replace([
                '{paragraph_top_parent:',
                '}',
              ], '', $arg);
              if ($parent instanceof FieldableEntityInterface && $parent->hasField($parentField)) {
                $queryArray[$key] = $parent->get($parentField)->getString();
              }
            }

          }
        }
        $args['args'] = http_build_query($queryArray);
      }

    }
    yield $args['id'] . (isset($args['args']) ? '?' . $args['args'] : '');
  }

}
