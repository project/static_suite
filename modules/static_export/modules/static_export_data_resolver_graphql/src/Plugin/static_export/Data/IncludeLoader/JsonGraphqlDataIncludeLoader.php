<?php

namespace Drupal\static_export_data_resolver_graphql\Plugin\static_export\Data\IncludeLoader;

use Drupal\static_export\Exporter\Data\Includes\Loader\DataIncludeLoaderPluginBase;

/**
 * Provides a JSON data include loader for data coming from GraphQL.
 *
 * @StaticDataIncludeLoader(
 *  id = "json-graphql",
 *  label = "JSON",
 *  description = "JSON data include loader for data coming from GraphQL",
 *  mimetype = "application/json"
 * )
 */
class JsonGraphqlDataIncludeLoader extends DataIncludeLoaderPluginBase {

  protected const FORMAT = 'json';
  protected const REGEXP_PATTERN = '/"([^"]*(entity|config|locale|custom|query))Include"\s*:\s*"([^"]+)"/i';
  protected const INCLUDE_FALLBACK = 'null';
  protected const ENTITY_INCLUDE_DATA_PATTERN = '/^{"data":{"content":{(.+)}}(,"metadata":{.*})?}$/U';
  protected const ENTITY_INCLUDE_FALLBACK_START_CHAR = '{';
  protected const ENTITY_INCLUDE_FALLBACK_END_CHAR = '}';
  protected const QUERY_INCLUDE_DATA_PATTERN = '/^{"data":(.+),"metadata":{.*}}$/';

  /**
   * {@inheritdoc}
   */
  protected function getConfig(): array {
    return [
      'format' => self::FORMAT,
      'regexpPattern' => self::REGEXP_PATTERN,
      'includeFallback' => self::INCLUDE_FALLBACK,
      'entityIncludeDataPattern' => self::ENTITY_INCLUDE_DATA_PATTERN,
      'entityIncludeFallbackStartChar' => self::ENTITY_INCLUDE_FALLBACK_START_CHAR,
      'entityIncludeFallbackEndChar' => self::ENTITY_INCLUDE_FALLBACK_END_CHAR,
      'queryIncludeDataPattern' => self::QUERY_INCLUDE_DATA_PATTERN,
    ];
  }

  /**
   * {@inheritdoc}
   *
   * Check if data is pretty printed, with multiple line breaks. If true,
   * re-encode it without pretty-printing it.
   */
  protected function sanitizeData(string $data): string {
    if (substr_count($data, "\n") > 1) {
      try {
        $dataArray = json_decode($data, TRUE, 512, JSON_THROW_ON_ERROR);
        $data = json_encode($dataArray, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
      }
      catch (\Exception $e) {
        @trigger_error('Error while sanitizing data in JSON GraphQL data include loader: ' . $e->getMessage(), E_USER_WARNING);
      }
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  protected function isDataSupported(string $data): bool {
    return str_starts_with($data, '{"data":{') && preg_match(self::REGEXP_PATTERN, $data);
  }

  /**
   * {@inheritdoc}
   */
  protected function addIncludeAlias(string $includeString, string $includeType, string $includeData) :string {
    [$includeKey] = explode(":", $includeString);
    // Keep "config" and "locale" string in resulting alias.
    // @see DataIncludeLoaderPluginBase::addIncludeAlias() documentation for
    // further detail.
    $pattern = in_array($includeType, ['config', 'locale']) ? '/Include"$/i' : '/' . $includeType . 'Include"$/i';
    $includeKey = preg_replace($pattern, '"', $includeKey);
    if ($includeKey === '""') {
      $includeKey = '"' . strtolower($includeType) . '"';
    }
    if ($includeType === 'query' && $includeData && $includeData !== 'null') {
      $includeData = '{"data":' . $includeData . '}';
    }
    return $includeKey . ':' . ($includeData ?: "null");
  }

}
