<?php

namespace Drupal\static_export_data_resolver_graphql\Plugin\static_export\Data\IncludeLoader;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\static_export\Exporter\Data\Includes\Loader\DataIncludeLoaderPluginBase;
use Drupal\static_export\Exporter\Output\Uri\UriFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Provides a XML data include loader for data coming from GraphQL.
 *
 * @StaticDataIncludeLoader(
 *  id = "xml-graphql",
 *  label = "XML",
 *  description = "XML data include loader for data coming from GraphQL",
 *  mimetype = "text/xml"
 * )
 */
class XmlGraphqlDataIncludeLoader extends DataIncludeLoaderPluginBase {

  protected const FORMAT = 'xml';
  protected const REGEXP_PATTERN = '/(<[^<]*(entity|config|locale|custom|query)Include>)([^<]+)(<\/[^<]*(entity|config|locale|custom|query)Include>)/i';
  protected const INCLUDE_FALLBACK = '';
  protected const ENTITY_INCLUDE_DATA_PATTERN = '/^<content>(.+)<\/content>$/';
  protected const ENTITY_INCLUDE_FALLBACK_START_CHAR = '';
  protected const ENTITY_INCLUDE_FALLBACK_END_CHAR = '';
  protected const QUERY_INCLUDE_DATA_PATTERN = '/^{"data":"(.+)","metadata":{.*}}$/';

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected SerializerInterface $serializer;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   Serializer.
   * @param \Drupal\static_export\Exporter\Output\Uri\UriFactoryInterface $uriFactory
   *   The URI factory.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory, EventDispatcherInterface $eventDispatcher, SerializerInterface $serializer, UriFactoryInterface $uriFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $configFactory, $eventDispatcher, $uriFactory);
    $this->serializer = $serializer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('event_dispatcher'),
      $container->get("serializer"),
      $container->get("static_export.uri_factory")
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getConfig(): array {
    return [
      'format' => self::FORMAT,
      'regexpPattern' => self::REGEXP_PATTERN,
      'includeFallback' => self::INCLUDE_FALLBACK,
      'entityIncludeDataPattern' => self::ENTITY_INCLUDE_DATA_PATTERN,
      'entityIncludeFallbackStartChar' => self::ENTITY_INCLUDE_FALLBACK_START_CHAR,
      'entityIncludeFallbackEndChar' => self::ENTITY_INCLUDE_FALLBACK_END_CHAR,
      'queryIncludeDataPattern' => self::QUERY_INCLUDE_DATA_PATTERN,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function isDataSupported(string $data): bool {
    return strpos($data, '<?xml') === 0 && preg_match(self::REGEXP_PATTERN, $data);
  }

  /**
   * {@inheritdoc}
   *
   * Removes useless XML declaration and <response> redundant element.
   */
  protected function loadInclude($url, array $parents): ?string {
    $includeData = parent::loadInclude($url, $parents);
    if ($includeData) {
      $includeData = str_replace('<?xml version="1.0"?>', '', $includeData);
      $includeData = preg_replace("/<data>(.+)<\/data>$/", "$1", $includeData);
    }
    return $includeData;
  }

  /**
   * {@inheritdoc}
   */
  protected function addIncludeAlias(string $includeString, string $includeType, string $includeData) :string {
    [$includeKey] = explode(">", $includeString);
    // Keep "config" and "locale" string in resulting alias.
    // @see DataIncludeLoaderPluginBase::addIncludeAlias() documentation for
    // further detail.
    $pattern = in_array($includeType, ['config', 'locale']) ? '/Include"$/i' : '/' . $includeType . 'Include"$/i';
    $includeKey = preg_replace($pattern, '', str_replace(['>', '<'], '', $includeKey));
    if ($includeKey === '') {
      $includeKey = strtolower($includeType);
    }
    if ($includeType === 'query' && $includeData && $includeData !== 'null') {
      $includeData = '<data>' . $includeData . '</data>';
    }
    return '<' . $includeKey . '>' . ($includeData ?: '') . '</' . $includeKey . '>';
  }

}
