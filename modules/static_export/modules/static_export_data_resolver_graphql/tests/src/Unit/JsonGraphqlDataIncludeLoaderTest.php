<?php

namespace Drupal\Tests\static_export_data_resolver_graphql\Unit;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\static_export\Exporter\Output\Uri\Uri;
use Drupal\static_export\Exporter\Output\Uri\UriFactoryInterface;
use Drupal\static_export_data_resolver_graphql\Plugin\static_export\Data\IncludeLoader\JsonGraphqlDataIncludeLoader;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Tests for json-graphql data include loader.
 *
 * @group static_suite:static_export_data_resolver_graphql
 */
class JsonGraphqlDataIncludeLoaderTest extends UnitTestCase {

  public const ENTITY = [
    'testData' => [
      'data' => [
        'content' => [
          'paragraph' => [
            'relation' => [
              "entityInclude" => "file://entity.json",
            ],
          ],
        ],
      ],
    ],
    'expectedData' => [
      'data' => [
        'content' => [
          'paragraph' => [
            'relation' => [
              "testKey" => "testValue",
            ],
          ],
        ],
      ],
    ],
  ];

  public const ENTITY_WITH_METADATA_VALUES = [
    'testData' => [
      'data' => [
        'content' => [
          'paragraph' => [
            'relation' => [
              "entityInclude" => "file://entity-with-metadata.json",
            ],
          ],
        ],
      ],
    ],
    'expectedData' => [
      'data' => [
        'content' => [
          'paragraph' => [
            'relation' => [
              "testKey" => "testValue",
            ],
          ],
        ],
      ],
    ],
  ];

  public const ENTITY_WITH_NULL_DATA = [
    'testData' => [
      'data' => [
        'content' => [
          'paragraph' => [
            'relation' => [
              "entityInclude" => "file://null.json",
            ],
          ],
        ],
      ],
    ],
    'expectedData' => [
      'data' => [
        'content' => [
          'paragraph' => [
            'relation' => NULL,
          ],
        ],
      ],
    ],
  ];

  public const CONFIG = [
    'testData' => [
      'data' => [
        'content' => [
          "configInclude" => "file://config.json",
        ],
      ],
    ],
    'expectedData' => [
      'data' => [
        'content' => [
          "config" => [
            "testKey" => "testValue",
          ],
        ],
      ],
    ],
  ];

  public const CONFIG_WITH_ALIAS = [
    'testData' => [
      'data' => [
        'content' => [
          "myAliasConfigInclude" => "file://config.json",
        ],
      ],
    ],
    'expectedData' => [
      'data' => [
        'content' => [
          "myAliasConfig" => [
            "testKey" => "testValue",
          ],
        ],
      ],
    ],
  ];

  public const LOCALE = [
    'testData' => [
      'data' => [
        'content' => [
          "localeInclude" => "file://locale.json",
        ],
      ],
    ],
    'expectedData' => [
      'data' => [
        'content' => [
          "locale" => [
            "testKey" => "testValue",
          ],
        ],
      ],
    ],
  ];

  public const LOCALE_WITH_ALIAS = [
    'testData' => [
      'data' => [
        'content' => [
          "myAliasLocaleInclude" => "file://locale.json",
        ],
      ],
    ],
    'expectedData' => [
      'data' => [
        'content' => [
          "myAliasLocale" => [
            "testKey" => "testValue",
          ],
        ],
      ],
    ],
  ];

  public const CUSTOM = [
    'testData' => [
      'data' => [
        'content' => [
          "customInclude" => "file://custom.json",
        ],
      ],
    ],
    'expectedData' => [
      'data' => [
        'content' => [
          "custom" => [
            "testKey" => "testValue",
          ],
        ],
      ],
    ],
  ];

  public const CUSTOM_WITH_ALIAS = [
    'testData' => [
      'data' => [
        'content' => [
          "myAliasCustomInclude" => "file://custom.json",
        ],
      ],
    ],
    'expectedData' => [
      'data' => [
        'content' => [
          "myAlias" => [
            "testKey" => "testValue",
          ],
        ],
      ],
    ],
  ];

  public const QUERY = [
    'testData' => [
      'data' => [
        'content' => [
          "queryInclude" => "file://query.json",
        ],
      ],
    ],
    'expectedData' => [
      'data' => [
        'content' => [
          "query" => NULL,
        ],
      ],
    ],
  ];

  public const QUERY_WITH_ALIAS = [
    'testData' => [
      'data' => [
        'content' => [
          "myAliasQueryInclude" => "file://query.json",
        ],
      ],
    ],
    'expectedData' => [
      'data' => [
        'content' => [
          "myAlias" => NULL,
        ],
      ],
    ],
  ];
  use ProphecyTrait;

  /**
   * JsonGraphqlDataIncludeLoader Plugin.
   *
   * @var \Drupal\static_export_data_resolver_graphql\Plugin\static_export\Data\IncludeLoader\JsonGraphqlDataIncludeLoader
   */
  protected JsonGraphqlDataIncludeLoader $jsonGraphqlDataIncludeLoader;

  /**
   * Initialization of the dependencies.
   */
  protected function setUp(): void {
    parent::setUp();

    $container = new ContainerBuilder();

    $configFactory = $this->getConfigFactoryStub([
      'static_export_data_resolver_graphql.settings' => [
        'data_server_endpoint' => 'urlDataServer',
      ],
    ]);

    $eventDispatcher = $this->prophesize(EventDispatcherInterface::class);
    $eventDispatcher->dispatch(Argument::any(), Argument::any())
      ->will(function ($event) {
        return $event[0];
      });

    $uriFactory = $this->prophesize(UriFactoryInterface::class);
    $uriFactory->create(Argument::any())
      ->will(function ($file) {
        if (str_contains($file[0], 'metadata')) {
          return new Uri('file://', dirname(__DIR__) . '/../fixtures/entity-with-metadata.json');
        }
        if (str_contains($file[0], 'entity')) {
          return new Uri('file://', dirname(__DIR__) . '/../fixtures/entity.json');
        }
        if (str_contains($file[0], 'null')) {
          return new Uri('file://', dirname(__DIR__) . '/../fixtures/null.json');
        }
        return new Uri('file://', dirname(__DIR__) . '/../fixtures/other.json');
      });

    $this->jsonGraphqlDataIncludeLoader = new JsonGraphqlDataIncludeLoader([], '', '', $configFactory, $eventDispatcher->reveal(), $uriFactory->reveal());

    \Drupal::setContainer($container);
  }

  /**
   * Provides a list of elements to test for testReplaceAndAliasSystem().
   *
   * @return array
   *   The data for each test scenario.
   */
  public static function providerElements(): array {
    return [
      self::ENTITY,
      self::ENTITY_WITH_METADATA_VALUES,
      self::ENTITY_WITH_NULL_DATA,
      self::CONFIG,
      self::CONFIG_WITH_ALIAS,
      self::LOCALE,
      self::LOCALE_WITH_ALIAS,
      self::CUSTOM,
      self::CUSTOM_WITH_ALIAS,
      self::QUERY,
      self::QUERY_WITH_ALIAS,
    ];
  }

  /**
   * Test replace and alias system.
   *
   * @dataProvider providerElements
   *
   * @throws \JsonException
   */
  public function testReplaceAndAliasSystem($content, $expected): void {
    $json = json_encode($content, JSON_THROW_ON_ERROR);

    $result = $this->jsonGraphqlDataIncludeLoader->load($json);

    $this->assertEquals(
      json_encode($expected, JSON_THROW_ON_ERROR),
      $result
    );
  }

}
