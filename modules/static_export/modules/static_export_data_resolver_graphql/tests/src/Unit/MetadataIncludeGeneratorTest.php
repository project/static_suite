<?php

namespace Drupal\Tests\static_export_data_resolver_graphql\Unit;

use Drupal\static_export_data_resolver_graphql\MetadataIncludeGenerator;
use Drupal\Tests\UnitTestCase;

/**
 * Tests for metadata include generator.
 *
 * @group static_suite:static_export_data_resolver_graphql
 */
class MetadataIncludeGeneratorTest extends UnitTestCase {

  public const ENTITY_WITH_ALL_INCLUDES = [
    'testData' => [
      'data' => [
        'content' => [
          'paragraph' => [
            'relation' => [
              "entityInclude" => "file://entity.json",
              "configInclude" => "file://config.json",
              "localeInclude" => "file://locale.json",
              "customInclude" => "file://custom.json",
              "queryInclude" => "file://query.json",
            ],
          ],
        ],
      ],
    ],
    'expectedData' => [
      'data' => [
        'content' => [
          'paragraph' => [
            'relation' => [
              "entityInclude" => "file://entity.json",
              "configInclude" => "file://config.json",
              "localeInclude" => "file://locale.json",
              "customInclude" => "file://custom.json",
              "queryInclude" => "file://query.json",
            ],
          ],
        ],
      ],
      'metadata' => [
        'includes' => [
          'static' => [
            'data.content.paragraph.relation.entityInclude' => 'file://entity.json',
            'data.content.paragraph.relation.configInclude' => 'file://config.json',
            'data.content.paragraph.relation.localeInclude' => 'file://locale.json',
            'data.content.paragraph.relation.customInclude' => 'file://custom.json',
          ],
          'dynamic' => [
            'data.content.paragraph.relation.queryInclude' => 'file://query.json',
          ],
        ],
      ],
    ],
  ];

  /**
   * Provides a list of elements to test.
   *
   * @return array
   *   The data for each test scenario.
   */
  public static function providerElements(): array {
    return [
      self::ENTITY_WITH_ALL_INCLUDES,
    ];
  }

  /**
   * Test metadata include generator.
   *
   * @dataProvider providerElements
   */
  public function testMetadataIncludeGenerator($content, $expected): void {
    $metadataIncludeGenerator = new MetadataIncludeGenerator();

    $result = $metadataIncludeGenerator->addIncludeMetas($content);

    $this->assertEquals(
      $expected,
      $result
    );
  }

}
