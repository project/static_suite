<?php

namespace Drupal\static_export_exporter_raw_config\Commands;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\static_export\Exporter\Type\Custom\CustomExporterPluginManagerInterface;
use Drupal\static_export\File\FileCollectionFormatter;
use Drupal\static_suite\StaticSuiteUserException;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file to export raw_config data.
 */
class RawConfigCommands extends DrushCommands {

  /**
   * Custom exporter manager.
   *
   * @var \Drupal\static_export\Exporter\Type\Custom\CustomExporterPluginManagerInterface
   */
  protected CustomExporterPluginManagerInterface $customExporterPluginManager;

  /**
   * File collection formatter.
   *
   * @var \Drupal\static_export\File\FileCollectionFormatter
   */
  protected FileCollectionFormatter $fileCollectionFormatter;

  /**
   * Constructor.
   *
   * @param \Drupal\static_export\Exporter\Type\Custom\CustomExporterPluginManagerInterface $customExporterPluginManager
   *   Custom exporter manager.
   * @param \Drupal\static_export\File\FileCollectionFormatter $fileCollectionFormatter
   *   File collection formatter.
   */
  public function __construct(CustomExporterPluginManagerInterface $customExporterPluginManager, FileCollectionFormatter $fileCollectionFormatter) {
    parent::__construct();
    $this->customExporterPluginManager = $customExporterPluginManager;
    $this->fileCollectionFormatter = $fileCollectionFormatter;
  }

  /**
   * Exports raw_config from configured paths.
   *
   * @param array $execOptions
   *   An associative array of options whose values come
   *   from cli, aliases, config, etc.
   *
   * @command static-export:raw-config
   *
   * @static_export Annotation for drush hooks.
   * @static_export_data_dir_write Annotation for commands that write data.
   */
  public function exportRawConfig(
    array $execOptions = [
      'standalone' => FALSE,
      'log-to-file' => TRUE,
      'lock' => TRUE,
      'build' => FALSE,
    ]
  ) {
    $timeStart = microtime(TRUE);
    try {
      $raw_configExporter = $this->customExporterPluginManager->createInstance('raw_config');
      $raw_configExporter->setMustRequestBuild($execOptions['build']);
      $fileCollectionGroup = $raw_configExporter->export(
        [],
        $execOptions['standalone'],
        $execOptions['log-to-file'],
        $execOptions['lock']
      );
      foreach ($fileCollectionGroup->getFileCollections() as $fileCollection) {
        $this->fileCollectionFormatter->setFileCollection($fileCollection);
        $this->output()
          ->writeln($this->fileCollectionFormatter->getTextLines());
      }
      $this->output()
        ->writeln("TOTAL TIME: " . (round(microtime(TRUE) - $timeStart, 3)) . " secs.");
    }
    catch (StaticSuiteUserException | PluginException $e) {
      $this->logger()->error($e->getMessage());
    }
    catch (\Throwable $e) {
      $this->logger()->error($e);
    }
  }

}
