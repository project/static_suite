<?php

namespace Drupal\static_export_exporter_raw_config\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\static_export\Event\StaticExportEvent;
use Drupal\static_export\Event\StaticExportEvents;
use Drupal\static_export\Exporter\ExporterPluginBase;
use Drupal\static_export\Exporter\ExporterPluginInterface;
use Drupal\static_export\Exporter\Type\Custom\CustomExporterPluginManagerInterface;
use Drupal\static_export\File\FileCollectionGroup;
use Drupal\static_export_exporter_raw_config\Utils\RawConfigUtils;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for raw_config exporter.
 *
 * Exports raw_config data when any configured path is
 * modified.
 */
class EventSubscriber implements EventSubscriberInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The custom exporter manager.
   *
   * @var \Drupal\static_export\Exporter\Type\Custom\CustomExporterPluginManagerInterface
   */
  protected CustomExporterPluginManagerInterface $customExporterManager;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Constructor for exporter.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language manager.
   * @param \Drupal\static_export\Exporter\Type\Custom\CustomExporterPluginManagerInterface $customExporterManager
   *   The custom exporter manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LanguageManagerInterface $languageManager,
    CustomExporterPluginManagerInterface $customExporterManager
  ) {
    $this->configFactory = $config_factory;
    $this->languageManager = $languageManager;
    $this->customExporterManager = $customExporterManager;
  }

  /**
   * {@inheritdoc}
   */
  protected function getExporterId(): string {
    return 'raw_config';
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[StaticExportEvents::WRITE_START][] = ['onWriteStarts'];
    return $events;
  }

  /**
   * Exports a raw config (only one file).
   *
   * @param \Drupal\static_export\Exporter\ExporterPluginInterface $eventEntityExporter
   *   The event entity exporter.
   * @param array $updatedConfig
   *   Current changed config files array with path on key and content on value.
   *
   * @return \Drupal\static_export\File\FileCollectionGroup
   *   A fileCollectionGroup.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\static_suite\StaticSuiteException
   */
  protected function exportRawConfig(ExporterPluginInterface $eventEntityExporter, array $updatedConfig = []): FileCollectionGroup {
    $rawConfigExporter = $this->customExporterManager->getInstance(['plugin_id' => $this->getExporterId()]);
    return $rawConfigExporter->makeSlaveOf($eventEntityExporter)
      ->export(
        $updatedConfig,
        TRUE,
        $eventEntityExporter instanceof ExporterPluginBase ? $eventEntityExporter->mustLogToFile() : TRUE,
        $eventEntityExporter instanceof ExporterPluginBase ? $eventEntityExporter->isLock() : TRUE
      );
  }

  /**
   * Reacts to a StaticExportEvents::WRITE_START event.
   *
   * @param \Drupal\static_export\Event\StaticExportEvent $event
   *   The Static Export event.
   *
   * @return \Drupal\static_export\Event\StaticExportEvent
   *   The processed event.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\static_suite\StaticSuiteException
   */
  public function onWriteStarts(StaticExportEvent $event) {
    $eventExporter = $event->getExporter();

    if (
      $eventExporter->isMasterExport() &&
      !$eventExporter->isStandalone() &&
      $eventExporter->getPluginId() !== 'raw_config'
    ) {
      $config = $this->configFactory->get('static_export_exporter_raw_config.settings');
      $files = RawConfigUtils::getFilesConfig($config->get('files'), $this->languageManager->getDefaultLanguage()->getId());
      $updatedConfig = [];
      foreach ($event->getFileCollection()->getFileItems() as $fileItem) {
        foreach ($files as $key => $file) {
          if (strstr(
              $fileItem->getFilePath(),
              $file
            ) !== FALSE) {
            $updatedConfig[$key] = $fileItem->getFileContents();
            break;
          }
        }
      }
      if (count($updatedConfig) > 0) {
        $fileCollectionGroup = $this->exportRawConfig($eventExporter, ['updated_config' => $updatedConfig]);
        $event->getFileCollection()
          ->mergeMultiple($fileCollectionGroup->getFileCollections());
      }
    }
    return $event;
  }

}
