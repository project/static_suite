<?php

namespace Drupal\static_export_exporter_raw_config\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for RawConfig exporter.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'static_export_exporter_raw_config_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['static_export_exporter_raw_config.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('static_export_exporter_raw_config.settings');

    $form['help'] = [
      '#markup' =>
      '<p>' . $this->t('The raw config file is composed of the contents of the following files.<br /> Enter one value per line, in the format key|file-path.<br /> The key is optional: if a line contains a single string, it will be used as key and path.') . '</p>',
    ];

    $form['files'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Files'),
      '#required' => TRUE,
      '#description' => $this->t('One file per line, format key|file-path<br />available tokens: [langcode] for current language code'),
      '#default_value' => is_array($config->get('files')) ? implode("\n", $config->get('files')) : $config->get('files'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('static_export_exporter_raw_config.settings');
    $files = $form_state->getValue('files');

    $config->set('files', $this->cleanArrayInput($files));

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Transforms a string into an array and cleans it.
   *
   * @param string $value
   *   A string value with line breaks.
   *
   * @return array
   *   An array obtained from the given string.
   */
  protected function cleanArrayInput(string $value): array {
    $array = explode("\n", $value);
    $array = array_map('trim', $array);
    return array_filter($array);
  }

}
