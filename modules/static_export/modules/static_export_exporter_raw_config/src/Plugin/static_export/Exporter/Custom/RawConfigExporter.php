<?php

namespace Drupal\static_export_exporter_raw_config\Plugin\static_export\Exporter\Custom;

use Drupal\static_export\Exporter\Output\Config\ExporterOutputConfigInterface;
use Drupal\static_export\Exporter\Output\Uri\UriFactoryInterface;
use Drupal\static_export\Exporter\Type\Custom\CustomExporterPluginBase;
use Drupal\static_export_exporter_raw_config\Utils\RawConfigUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * RawConfig rules exporter.
 *
 * @StaticCustomExporter(
 *  id = "raw_config",
 *  label = @Translation("Raw config exporter"),
 *  description = @Translation("Export raw_config from given files."),
 * )
 */
class RawConfigExporter extends CustomExporterPluginBase {

  /**
   * The URI factory.
   *
   * @var \Drupal\static_export\Exporter\Output\Uri\UriFactoryInterface
   */
  protected UriFactoryInterface $uriFactory;

  /**
   * {@inheritdoc}
   */
  public function getExporterItem(): string {
    return 'raw_config';
  }

  /**
   * {@inheritdoc}
   */
  public function getExporterItemId(): string {
    return 'raw_config';
  }

  /**
   * {@inheritdoc}
   */
  public function getExporterItemLabel(): string {
    return "RawConfig exporter";
  }

  /**
   * {@inheritdoc}
   */
  protected function setExtraDependencies(ContainerInterface $container): void {
    $this->uriFactory = $container->get("static_export.uri_factory");
  }

  /**
   * {@inheritdoc}
   */
  protected function getOutputDefinition(): ExporterOutputConfigInterface {
    return $this->exporterOutputConfigFactory->create(
      'config',
      'raw-config',
      'json',
      $this->languageManager->getDefaultLanguage()
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \JsonException
   */
  protected function calculateDataFromResolver(): array {
    $updatedKeys = [];
    if (isset($this->options['updated_config'])) {
      $updatedKeys = array_keys($this->options['updated_config']);
    }
    $config = $this->configFactory->get(
      'static_export_exporter_raw_config.settings'
    );
    $files = RawConfigUtils::getFilesConfig(
      $config->get('files'),
      $this->languageManager->getDefaultLanguage()->getId()
    );
    $rawConfig = [];
    foreach ($files as $key => $file) {
      $uri = $this->uriFactory->create($file);
      if (in_array($key, $updatedKeys)) {
        $rawConfig[$key] = json_decode($this->options['updated_config'][$key]);
      }
      elseif (file_exists($uri)) {
        if (strrpos($file, '.json') + 5 === strlen($file)) {
          $rawConfig[$key] = json_decode(@file_get_contents($uri));
        }
        else {
          $rawConfig[$key] = ['value' => @file_get_contents($uri)];
        }
      }
    }

    // Generate a unique hash based on the contents of this config.
    $hash = md5(json_encode($rawConfig, JSON_THROW_ON_ERROR));
    $rawConfig['hash'] = $hash;

    return $rawConfig;
  }

}
