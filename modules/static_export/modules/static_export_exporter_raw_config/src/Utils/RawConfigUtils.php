<?php

namespace Drupal\static_export_exporter_raw_config\Utils;

/**
 * RawConfig Utils class.
 */
class RawConfigUtils {

  /**
   * Get parsed files config.
   *
   * @param array $filesConfig
   *   File config array.
   * @param string $langcode
   *   Langcode.
   *
   * @return array
   *   The parsed config.
   */
  public static function getFilesConfig(array $filesConfig, string $langcode): array {
    $parsedConfig = [];
    foreach ($filesConfig as $fileConfig) {
      $fileConfigArray = explode('|', str_replace('[langcode]', $langcode, $fileConfig));
      $value = $fileConfigArray[0];
      if (count($fileConfigArray) > 1) {
        $value = $fileConfigArray[1];
      }
      $parsedConfig[$fileConfigArray[0]] = $value;
    }
    return $parsedConfig;
  }

}
