<?php

namespace Drupal\Tests\static_export_output_formatter_json\Unit;

use Drupal\static_export_output_formatter_json\Plugin\static_export\Output\Formatter\JsonOutputFormatter;
use Drupal\Tests\UnitTestCase;

/**
 * Tests for json output formatter.
 *
 * @group static_suite:static_export_output_formatter_json
 */
class JsonOutputFormatterTest extends UnitTestCase {

  public const DATA_TO_PROCESS = [
    'data' => [
      'content' => [
        "testKey" => "testValue",
      ],
    ],
  ];

  /**
   * Test json output formatter.
   *
   * @throws \JsonException
   */
  public function testJsonOutputFormatter(): void {
    $correctlyFormattedJSON = json_encode(self::DATA_TO_PROCESS, JSON_THROW_ON_ERROR);

    $configFactory = $this->getConfigFactoryStub([
      'static_export_output_formatter_json.settings' => [
        'pretty_print' => FALSE,
      ],
    ]);

    $jsonOutputFormatter = new JsonOutputFormatter([], '', '', $configFactory);

    $this->assertEquals(
      $correctlyFormattedJSON,
      $jsonOutputFormatter->format(self::DATA_TO_PROCESS)
    );
  }

  /**
   * Test json output formatter with pretty print.
   *
   * @throws \JsonException
   */
  public function testJsonOutputFormatterWithPrettyPrint(): void {
    $correctlyFormattedJsonWithPretty = json_encode(self::DATA_TO_PROCESS, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT);

    $configFactory = $this->getConfigFactoryStub([
      'static_export_output_formatter_json.settings' => [
        'pretty_print' => TRUE,
      ],
    ]);

    $jsonOutputFormatter = new JsonOutputFormatter([], '', '', $configFactory);

    $this->assertEquals(
      $correctlyFormattedJsonWithPretty,
      $jsonOutputFormatter->format(self::DATA_TO_PROCESS)
    );
  }

}
