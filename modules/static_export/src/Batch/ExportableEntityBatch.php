<?php

namespace Drupal\static_export\Batch;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\static_export\Entity\ExportableEntityInterface;
use Drupal\static_suite\Entity\EntityUtilsInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * ExportableEntityBatch service.
 *
 * It allows to safely remove an exportable entity, removing all related
 * exported files.
 */
class ExportableEntityBatch implements ExportableEntityBatchInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The static suite entity utils service.
   *
   * @var \Drupal\static_suite\Entity\EntityUtilsInterface
   */
  protected EntityUtilsInterface $entityUtils;

  /**
   * Constructs an ExportableEntityBatch object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel to log errors.
   * @param \Drupal\static_suite\Entity\EntityUtilsInterface $entityUtils
   *   The static suite entity utils service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $loggerChannelFactory, EntityUtilsInterface $entityUtils) {
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $loggerChannelFactory->get('static_export');
    $this->entityUtils = $entityUtils;
  }

  /**
   * {@inheritdoc}
   */
  public function export(ExportableEntityInterface $exportableEntity): void {
    // Obtain entities related to this exportable entity.
    $entityTypeId = $exportableEntity->getEntityTypeIdString();
    $options = [
      'bundle' => $exportableEntity->getBundle(),
    ];
    try {
      $exportedEntitiesIds = $this->entityUtils->getEntityIds(
        $entityTypeId,
        $options
      );
      if (count($exportedEntitiesIds)) {
        $exportedEntities = $this->entityTypeManager
          ->getStorage($entityTypeId)
          ->loadMultiple($exportedEntitiesIds);

        $batch = [
          'title' => $this->t('Exporting entities...'),
          'operations' => [],
          'init_message' => $this->t('Starting'),
          'progress_message' => $this->t('Processed @current out of @total.'),
          'error_message' => $this->t('An error occurred during processing'),
        ];

        foreach ($exportedEntities as $exportedEntity) {
          $batch['operations'][] = [
            [
              self::class,
              'exportEntity',
            ],
            [$exportedEntity],
          ];
        }
        $batch['operations'][] = [
          [
            self::class,
            'finishedExportingEntities',
          ],
          [$exportableEntity->label()],
        ];
        batch_set($batch);
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function delete(ExportableEntityInterface $exportableEntity): void {
    $exportableEntity->disable()->save();

    // Obtain entities related to this exportable entity.
    $entityTypeId = $exportableEntity->getEntityTypeIdString();
    $options = [
      'bundle' => $exportableEntity->getBundle(),
    ];
    try {
      $exportedEntitiesIds = $this->entityUtils->getEntityIds(
        $entityTypeId,
        $options
      );
      if (count($exportedEntitiesIds)) {
        $exportedEntities = $this->entityTypeManager
          ->getStorage($entityTypeId)
          ->loadMultiple($exportedEntitiesIds);

        $batch = [
          'title' => $this->t('Removing exported entities...'),
          'operations' => [],
          'init_message' => $this->t('Starting'),
          'progress_message' => $this->t('Processed @current out of @total.'),
          'error_message' => $this->t('An error occurred during processing'),
        ];

        foreach ($exportedEntities as $exportedEntity) {
          $batch['operations'][] = [
            [
              self::class,
              'deleteEntityExportedData',
            ],
            [$exportedEntity],
          ];
        }
        $batch['operations'][] = [
          [
            self::class,
            'deleteExportableEntity',
          ],
          [$exportableEntity->id()],
        ];

        batch_set($batch);
      }
      else {
        self::deleteExportableEntity($exportableEntity->id());
      }
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
  }

  /**
   * Export entity data.
   *
   * This is the callback for exportable entity batch export operations.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity whose data will be exported.
   */
  public static function exportEntity(EntityInterface $entity): void {
    $entityExporterPluginManager = \Drupal::service('plugin.manager.static_entity_exporter');
    $entityExporter = $entityExporterPluginManager->createDefaultInstance();
    $entityExporter->setMustRequestBuild(FALSE);
    $entityExporter->write(['entity' => $entity]);
  }

  /**
   * Callback to inform about the export operation of an exportable entity.
   *
   * @param string $label
   *   The label of an exportable entity.
   */
  public static function finishedExportingEntities(string $label): void {
    \Drupal::messenger()
      ->addMessage(t('Entity %label has been successfully exported.', ['%label' => $label]));
  }

  /**
   * Callback to remove the entity exported data.
   *
   * This is the callback for exportable entity batch delete operations.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity whose exported data will be deleted.
   */
  public static function deleteEntityExportedData(EntityInterface $entity): void {
    $entityExporterPluginManager = \Drupal::service('plugin.manager.static_entity_exporter');
    $entityExporter = $entityExporterPluginManager->createDefaultInstance();
    $entityExporter->setMustRequestBuild(FALSE);
    $entityExporter->delete(['entity' => $entity]);
  }

  /**
   * Callback to remove an exportable entity by id.
   *
   * @param string $id
   *   The id of an exportable entity.
   */
  public static function deleteExportableEntity(string $id): void {
    $entityUtils = \Drupal::service('static_suite.entity_utils');
    $exportableEntity = $entityUtils->loadEntity('exportable_entity', $id);
    $exportableEntity->delete();
    \Drupal::messenger()
      ->addMessage(t('Entity %label has been deleted.', ['%label' => $exportableEntity->label()]));
  }

}
