<?php

namespace Drupal\static_export\Batch;

use Drupal\static_export\Entity\ExportableEntityInterface;

/**
 * An interface for ExportableEntityBatch services.
 *
 * It allows to safely remove an exportable entity, removing all related
 * exported files.
 */
interface ExportableEntityBatchInterface {

  /**
   * Export all entities related to an exportable entity.
   *
   * @param \Drupal\static_export\Entity\ExportableEntityInterface $exportableEntity
   *   The exportable entity to be exported.
   */
  public function export(ExportableEntityInterface $exportableEntity): void;

  /**
   * Remove an exportable entity from database.
   *
   * It also removes all related files in filesystem.
   *
   * @param \Drupal\static_export\Entity\ExportableEntityInterface $exportableEntity
   *   The exportable entity to be deleted.
   */
  public function delete(ExportableEntityInterface $exportableEntity): void;

}
