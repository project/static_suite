<?php

namespace Drupal\static_export\Event;

/**
 * Contains all events dispatched by Data Include Loader.
 */
final class DataIncludeLoaderEvents {

  public const SANITIZED_DATA = 'data_include_loader.sanitized_data';

  public const IS_DATA_SUPPORTED = 'data_include_loader.is_data_supported';

  public const PARSED_DATA = 'data_include_loader.data_parsing_ends';

}
