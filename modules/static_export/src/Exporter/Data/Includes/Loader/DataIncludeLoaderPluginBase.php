<?php

namespace Drupal\static_export\Exporter\Data\Includes\Loader;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\static_export\Event\DataIncludeLoaderEvents;
use Drupal\static_export\Exporter\Output\Uri\UriFactoryInterface;
use Drupal\static_suite\Event\DataEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provides an abstract data include loader.
 */
abstract class DataIncludeLoaderPluginBase extends PluginBase implements DataIncludeLoaderPluginInterface, ContainerFactoryPluginInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The URI factory.
   *
   * @var \Drupal\static_export\Exporter\Output\Uri\UriFactoryInterface
   */
  protected UriFactoryInterface $uriFactory;

  /**
   * Config array.
   *
   * @var array
   */
  protected array $config;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   * @param \Drupal\static_export\Exporter\Output\Uri\UriFactoryInterface $uriFactory
   *   The URI factory.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory, EventDispatcherInterface $eventDispatcher, UriFactoryInterface $uriFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
    $this->eventDispatcher = $eventDispatcher;
    $this->uriFactory = $uriFactory;
    $this->config = $this->getConfig();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('event_dispatcher'),
      $container->get("static_export.uri_factory")
    );
  }

  /**
   * Sanitize data to ensure it matches a specific format.
   *
   * This method could take, for instance, a pretty-printed JSON and remove all
   * its line breaks, etc.
   *
   * Since not all loaders need this method, by default it simply returns the
   * received $data string.
   *
   * @param string $data
   *   The string to be sanitized.
   *
   * @return string
   *   The sanitized data.
   */
  protected function sanitizeData(string $data): string {
    return $data;
  }

  /**
   * Tells whether this data is supported.
   *
   * Data can arrive from different sources (GraphQL, JSON:API, custom
   * exporters, etc.) so we check if data is in a format this loader
   * understands.
   *
   * @param string $data
   *   The string to be parsed.
   *
   * @return bool
   *   True if data is supported
   */
  abstract protected function isDataSupported(string $data): bool;

  /**
   * Get config array.
   *
   * @return array
   *   Array with config values, keyed by:
   *   - 'format'
   *   - 'regexpPattern'
   *   - 'includeFallback'
   *   - 'entityIncludeDataPattern'
   *   - 'entityIncludeFallbackStartChar'
   *   - 'entityIncludeFallbackEndChar'
   *   - 'queryIncludeDataPattern'
   */
  abstract protected function getConfig(): array;

  /**
   * Find includes inside data.
   *
   * @param string $data
   *   Data to be parsed.
   *
   * @return array|null
   *   Array of matches, or null if nothing found.
   */
  protected function findIncludes(string $data): ?array {
    preg_match_all($this->config['regexpPattern'], $data, $matches);
    return $matches;
  }

  /**
   * {@inheritdoc}
   */
  public function load(string $data): string {
    $data = $this->sanitizeData($data);
    $event = new DataEvent(['sanitized-data' => $data]);
    $data = $this->eventDispatcher->dispatch($event, DataIncludeLoaderEvents::SANITIZED_DATA)->getDataItem('sanitized-data');

    // Load includes only for known data.
    $event = new DataEvent([
      'data' => $data,
      'is-data-supported' => $this->isDataSupported($data),
    ]);
    $isDataSupported = $this->eventDispatcher->dispatch($event, DataIncludeLoaderEvents::IS_DATA_SUPPORTED)->getDataItem('is-data-supported');
    if (!$isDataSupported) {
      return $data;
    }

    $parsedData = $this->parseData($data, [0 => 'ROOT']);
    $event = new DataEvent(['parsed-data' => $parsedData]);
    return $this->eventDispatcher->dispatch($event, DataIncludeLoaderEvents::PARSED_DATA)->getDataItem('parsed-data');
  }

  /**
   * Calculate the alias and adds it to the string being parsed.
   *
   * Different data includes produce a different alias:
   * - entityInclude: it is completely replaced by the included data
   * - configInclude or myAliasConfigInclude: replaced with "config" and
   * "myAliasConfig"
   * - localeInclude or myAliasLocaleInclude: replaced with "locale" and
   * "myAliasLocale"
   * - customInclude or myAliasCustomInclude: replaced with "custom" and
   * "myAlias" (without "custom", since it is a detail of internal
   * implementation)
   * - queryInclude or myAliasQueryInclude: replaced with "query" and
   * "myAlias" (without "query", since it is a detail of internal
   * implementation)
   *
   * @param string $includeString
   *   Complete include string.
   * @param string $includeType
   *   The type of the data include (entity, custom, locale, config, query...)
   * @param string $includeData
   *   A string with includes to be parsed.
   *
   * @return string
   *   The calculated alias
   */
  abstract protected function addIncludeAlias(string $includeString, string $includeType, string $includeData) :string;

  /**
   * Recursive parse data, parses data of the included data.
   *
   * @param string $includeData
   *   A string with includes to be parsed.
   * @param string $url
   *   Url of the include file.
   * @param array $parents
   *   An array with the hierarchy of paths from the main data file to where an
   *   include file appears.
   *
   * @return string
   *   The parsed string with includes loaded.
   */
  protected function recursiveParseData(string $includeData, string $url, array $parents) :string {
    if (!empty($includeData)) {
      // Recursively load other includes.
      $includeParents = $parents;
      $includeParents[] = $url;
      $includeData = $this->parseData($includeData, $includeParents);
    }
    return $includeData;
  }

  /**
   * Entity parse data.
   *
   * @param string $includeString
   *   Complete include string.
   * @param string $includePath
   *   A string with original path of the include file.
   * @param string $data
   *   A string with includes to be parsed.
   * @param array $parents
   *   An array with the hierarchy of paths from the main data file to where an
   *   include file appears.
   *
   * @return string
   *   The parsed string with includes loaded.
   */
  protected function parseDataEntity(string $includeString, string $includePath, string $data, array $parents) :string {
    $uri = $this->uriFactory->create($includePath);
    $includeData = $this->recursiveParseData($this->loadInclude($uri->getComposed(), $parents), $uri->getComposed(), $parents);
    if ($includeData &&
            ($includeDataWithoutExtraLevels = preg_replace($this->config['entityIncludeDataPattern'], "$1", $includeData)) &&
            ($includeDataWithoutExtraLevels !== $includeData)) {
      return trim(str_replace($includeString, $includeDataWithoutExtraLevels, $data));
    }
    $search = $this->config['entityIncludeFallbackStartChar'] . $includeString . $this->config['entityIncludeFallbackEndChar'];
    return trim(str_replace($search, $this->config['includeFallback'], $data));
  }

  /**
   * Parse data for query includes.
   *
   * @param string $includeString
   *   Complete include string.
   * @param string $includePath
   *   A string with original path of the include file.
   * @param string $data
   *   A string with includes to be parsed.
   * @param array $parents
   *   An array with the hierarchy of paths from the main data file to where an
   *   include file appears.
   *
   * @return string
   *   The parsed string with includes loaded.
   */
  protected function parseDataQuery(string $includeString, string $includePath, string $data, array $parents) : string {
    $dataServerEndpoint = $this->configFactory->get('static_export_data_resolver_graphql.settings')->get('data_server_endpoint');
    $includePath = stripcslashes($includePath);
    $url = $dataServerEndpoint . '/query/' . $includePath;
    $query = parse_url($url, PHP_URL_QUERY);
    $format = (empty($query) ? '?' : '&') . '_format=' . $this->config['format'];
    $url .= $format;
    $includeData = $this->recursiveParseData($this->loadInclude($url, $parents), $url, $parents);
    $replaceData = $this->config['includeFallback'];
    if ($includeData &&
    ($includeDataWithoutExtraLevels = preg_replace($this->config['queryIncludeDataPattern'], "$1", $includeData)) &&
    ($includeDataWithoutExtraLevels !== $includeData)) {
      $replaceData = $includeDataWithoutExtraLevels;
    }
    return trim(str_replace($includeString, $this->addIncludeAlias($includeString, 'query', $replaceData), $data));
  }

  /**
   * Parse data for config, locale and custom includes.
   *
   * @param string $includeString
   *   Complete include string.
   * @param string $includePath
   *   A string with original path of the include file.
   * @param string $data
   *   A string with includes to be parsed.
   * @param array $parents
   *   An array with the hierarchy of paths from the main data file to where an
   *   include file appears.
   * @param string $type
   *   The type of the data include (entity, custom, locale, config, query...)
   *
   * @return string
   *   The parsed string with includes loaded.
   */
  protected function parseDataOther(string $includeString, string $includePath, string $data, array $parents, string $type) :string {
    $uri = $this->uriFactory->create($includePath);
    $includeData = $this->recursiveParseData($this->loadInclude($uri->getComposed(), $parents), $uri->getComposed(), $parents);

    return trim(str_replace($includeString, $this->addIncludeAlias($includeString, $type, $includeData), $data));
  }

  /**
   * Parse data and load includes.
   *
   * We use a helper function so we can manage $this->loadedIncludes in a more
   * easier way.
   *
   * @param string $data
   *   A string with includes to be parsed.
   * @param array $parents
   *   An array with the hierarchy of paths from the main data file to where an
   *   include appears.
   *
   * @return string
   *   The parsed string with includes loaded.
   */
  protected function parseData(string $data, array $parents): string {
    $matches = $this->findIncludes($data);
    if ($matches) {
      foreach ($matches[3] as $key => $includePath) {
        $type = strtolower($matches[2][$key]);
        $includeString = $matches[0][$key];
        if (preg_match("/^.data\.content/", $includeString) !== 1) {
          $data = match($type) {
            'entity' => $this->parseDataEntity($includeString, $includePath, $data, $parents),
            'query' => $this->parseDataQuery($includeString, $includePath, $data, $parents),
            default => $this->parseDataOther($includeString, $includePath, $data, $parents, $type),
          };
        }
      }
    }
    return $data;
  }

  /**
   * Loads an include and mark it as loaded to avoid repeating it.
   *
   * Internally it uses isIncludeAlreadyLoaded() and markIncludeAsLoaded().
   *
   * @param string $url
   *   The include to be loaded.
   * @param array $parents
   *   An array with the hierarchy of paths from the main data file to where an
   *   include appears.
   *
   * @return string
   *   A string with includes loaded, or NULL if nothing found or include
   *   already loaded.
   */
  protected function loadInclude(string $url, array $parents): string {
    // Avoid loading an include twice.
    if ($this->isIncludeAlreadyLoaded($url, $parents)) {
      $includeData = '';
    }
    else {
      $includeData = $this->sanitizeData(trim(@file_get_contents($url)));
    }
    return $includeData;
  }

  /**
   * Tells whether an include is already loaded in the same parent hierarchy.
   *
   * @param string $url
   *   The file to be checked.
   * @param array $parents
   *   An array with the hierarchy of paths from the main data file to where an
   *   include appears.
   *
   * @return bool
   *   True if it has been already loaded, false otherwise.
   */
  protected function isIncludeAlreadyLoaded(string $url, array $parents): bool {
    return in_array($url, $parents, TRUE);
  }

}
