<?php

namespace Drupal\static_export\File;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\static_export\Entity\ExportableEntityManagerInterface;
use Drupal\static_export\Exporter\Type\Entity\Output\Uri\Resolver\EntityExporterUriResolverInterface;
use Drupal\system\MenuInterface;

/**
 * Exported files viewer.
 */
class ExportedFilesViewer implements ExportedFilesViewerInterface {

  use StringTranslationTrait;

  /**
   * The Current User object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The file url generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected FileUrlGeneratorInterface $fileUrlGenerator;

  /**
   * The exportable entity manager.
   *
   * @var \Drupal\static_export\Entity\ExportableEntityManagerInterface
   */
  protected ExportableEntityManagerInterface $exportableEntityManager;

  /**
   * The entity exporter uri resolver.
   *
   * @var \Drupal\static_export\Exporter\Type\Entity\Output\Uri\Resolver\EntityExporterUriResolverInterface
   */
  protected EntityExporterUriResolverInterface $entityExporterUriResolver;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The Current User object.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file url generator.
   * @param \Drupal\static_export\Entity\ExportableEntityManagerInterface $exportableEntityManager
   *   The exportable entity manager.
   * @param \Drupal\static_export\Exporter\Type\Entity\Output\Uri\Resolver\EntityExporterUriResolverInterface $entityExporterUriResolver
   *   The entity exporter uri resolver.
   */
  public function __construct(
    AccountInterface $currentUser,
    DateFormatterInterface $dateFormatter,
    FileUrlGeneratorInterface $fileUrlGenerator,
    ExportableEntityManagerInterface $exportableEntityManager,
    EntityExporterUriResolverInterface $entityExporterUriResolver
    ) {
    $this->currentUser = $currentUser;
    $this->dateFormatter = $dateFormatter;
    $this->fileUrlGenerator = $fileUrlGenerator;
    $this->exportableEntityManager = $exportableEntityManager;
    $this->entityExporterUriResolver = $entityExporterUriResolver;
  }

  /**
   * {@inheritdoc}
   */
  public function showInfo(EntityInterface $entity): array {
    if (!$this->isValidEntity($entity)) {
      return [];
    }

    $build = [];

    // Theme: create the advanced section.
    if (!isset($build['advanced'])) {
      $build['advanced'] = [
        '#type' => 'vertical_tabs',
        '#weight' => 99,
        '#accordion' => TRUE,
        '#attributes' => [
          'class' => ['entity-meta'],
        ],
      ];
    }

    // Create the group for the fields.
    $build['static_export_info'] = [
      '#type' => 'details',
      '#title' => $this->t('Static export'),
      '#access' => $this->currentUser->hasPermission('view static export entity form summary info'),
      '#open' => FALSE,
      '#group' => 'advanced',
      '#tree' => TRUE,
      '#weight' => -9,
    ];

    $exportedFilePaths = $this->entityExporterUriResolver->setEntity($entity)->getUris();

    $header = [
      'path' => $this->t('Path'),
      'modification_date' => $this->t('Modification date'),
      'size' => $this->t('Size (bytes)'),
    ];
    if ($this->currentUser->hasPermission('view static export files')) {
      $header['includes'] = $this->t('Data includes');
    }

    $options = [];
    foreach ($exportedFilePaths as $uri) {
      $exportedUriTarget = $uri->getTarget();
      $modificationTime = @filemtime($uri);
      if ($this->currentUser->hasPermission('view static export files')) {
        $pathData = [
          'data' => [
            // Add a Zero Width Space in each "/" to avoid breaking the table.
            '#title' => str_replace('/', '/​', $exportedUriTarget),
            '#type' => 'link',
            '#attributes' => ['target' => '_blank'],
            '#url' => $this->fileUrlGenerator->generate($uri),
          ],
        ];
      }
      else {
        $pathData = $exportedUriTarget;
      }
      $size = @filesize($uri);
      $row = [
        'path' => $pathData,
        'modification_date' => $modificationTime ? $this->dateFormatter->format($modificationTime, 'custom', 'Y-m-d H:i:s') : '--',
        'size' => $size ?: '--',
      ];
      if ($this->currentUser->hasPermission('view static export files')) {
        $row['includes'] = [
          'data' => [
            '#title' => $this->t('view'),
            '#type' => 'link',
            '#attributes' => ['target' => '_blank'],
            '#url' => Url::fromUri($this->fileUrlGenerator->generateAbsoluteString($uri), ['query' => ['includes' => TRUE]]),
          ],
        ];
      }

      $options[] = $row;
    }

    $build['static_export_info']['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $options,
      '#required' => FALSE,
      '#multiple' => FALSE,
      '#empty' => $this->t('No exported files found.'),
    ];

    return $build;
  }

  /**
   * Checks if entity is valid.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to be checked.
   *
   * @return bool
   *   True if it is valid, false otherwise.
   */
  protected function isValidEntity(EntityInterface $entity): bool {
    // Check if it is an exportable entity.
    if (!$entity->id() || !$this->exportableEntityManager->isExportable($entity)) {
      return FALSE;
    }

    // Check if entity is published.
    $isPublished = method_exists($entity, 'isPublished') ? $entity->isPublished() : FALSE;
    if ($entity instanceof MenuInterface) {
      $isPublished = TRUE;
    }

    if (!$isPublished) {
      return FALSE;
    }

    return TRUE;
  }

}
