<?php

namespace Drupal\static_export\File;

use Drupal\Core\Entity\EntityInterface;

/**
 * Exported files viewer interface.
 */
interface ExportedFilesViewerInterface {

  /**
   * Get exported files info as a render array.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   *
   * @return array
   *   The render array with exported files info.
   */
  public function showInfo(EntityInterface $entity): array;

}
