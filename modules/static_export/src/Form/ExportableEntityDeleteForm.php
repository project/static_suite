<?php

namespace Drupal\static_export\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\static_export\Batch\ExportableEntityBatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form to delete an "Exportable Entity".
 */
class ExportableEntityDeleteForm extends EntityConfirmFormBase {

  /**
   * The entity being used by this form.
   *
   * Locally define this is an ExportableEntityInterface instead of an
   * EntityInterface, so typing works consistently inside this class.
   *
   * Please note that we can not define it as
   * "protected ExportableEntityInterface $entity;" since its parent is not
   * using typing.
   *
   * @var \Drupal\static_export\Entity\ExportableEntityInterface
   */
  protected $entity;

  /**
   * The exportable entity batch interface.
   *
   * @var \Drupal\static_export\Batch\ExportableEntityBatchInterface
   */
  protected ExportableEntityBatchInterface $exportableEntityBatch;

  /**
   * Constructs a \Drupal\static_export\Form\ExportableEntityDeleteForm object.
   *
   * @param \Drupal\static_export\Batch\ExportableEntityBatchInterface $exportableEntityBatch
   *   The exportable entity batch interface.
   */
  public function __construct(ExportableEntityBatchInterface $exportableEntityBatch) {
    $this->exportableEntityBatch = $exportableEntityBatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ExportableEntityDeleteForm | static {
    return new static(
      $container->get('static_export.exportable_entity_batch'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['disable_builds_warning'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Warning'),
    ];
    $form['disable_builds_warning']['message'] = [
      '#markup' => $this->t('Deleting this entity will remove its exported data. It is recommended to <strong>disable build process</strong> before continuing.
    You can disable it in the <a target="_blank" href="@url">Static Build settings</a> page', [
      '@url' => Url::fromRoute('static_build.settings')
        ->toString(),
    ]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.exportable_entity.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->exportableEntityBatch->delete($this->entity);
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
