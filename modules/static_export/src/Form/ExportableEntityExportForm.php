<?php

namespace Drupal\static_export\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\static_export\Batch\ExportableEntityBatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form to export an "Exportable Entity".
 */
class ExportableEntityExportForm extends EntityConfirmFormBase {

  /**
   * The entity being used by this form.
   *
   * Locally define this is an ExportableEntityInterface instead of an
   * EntityInterface, so typing works consistently inside this class.
   *
   * Please note that we can not define it as
   * "protected ExportableEntityInterface $entity;" since its parent is not
   * using typing.
   *
   * @var \Drupal\static_export\Entity\ExportableEntityInterface
   */
  protected $entity;

  /**
   * The exportable entity batch interface.
   *
   * @var \Drupal\static_export\Batch\ExportableEntityBatchInterface
   */
  protected ExportableEntityBatchInterface $exportableEntityBatch;

  /**
   * Constructs a \Drupal\static_export\Form\ExportableEntityDeleteForm object.
   *
   * @param \Drupal\static_export\Batch\ExportableEntityBatchInterface $exportableEntityBatch
   *   The exportable entity batch interface.
   */
  public function __construct(ExportableEntityBatchInterface $exportableEntityBatch) {
    $this->exportableEntityBatch = $exportableEntityBatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ExportableEntityDeleteForm | static {
    return new static(
      $container->get('static_export.exportable_entity_batch'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $exportableEntity = $form_state->getFormObject()->getEntity();
    $form['warning'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Warning'),
      'message' => [
        '#markup' => $this->t('The export process will re-export all entities of type "%type", so it could take several minutes.', ['%type' => $exportableEntity->label()]),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to export %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * Return form cancel url.
   */
  public function getCancelUrl(): Url {
    return new Url('entity.exportable_entity.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Export');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->exportableEntityBatch->export($this->entity);
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
