<?php

namespace Drupal\Tests\static_export\Unit;

use Drupal\static_export\PathProcessor\PathProcessorFiles;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tests for path processor files.
 *
 * @group static_suite:static_export
 */
class PathProcessorFilesTest extends UnitTestCase {

  /**
   * Path processor files.
   *
   * @var \Drupal\static_export\PathProcessor\PathProcessorFiles
   */
  protected PathProcessorFiles $pathProcessorFiles;

  /**
   * Initialization of the dependencies.
   */
  protected function setUp(): void {
    parent::setUp();
    $this->pathProcessorFiles = new PathProcessorFiles();
  }

  /**
   * Returns the same path if the path don't have correct format.
   */
  public function testReturnsTheSamePathIfDontHaveCorrectFormat(): void {
    $path = 'invalid-format';
    $request = Request::create($path);

    $this->assertEquals(
      $path,
      $this->pathProcessorFiles->processInbound($path, $request)
    );
  }

  /**
   * Returns the same path if the path has correct but isset uri_target param.
   */
  public function testReturnsTheSamePathIfIssetUriTargetParam(): void {
    $path = '/static/export/files/valid-path';
    $request = Request::create($path, 'GET', ['uri_target' => FALSE]);

    $this->assertEquals(
      $path,
      $this->pathProcessorFiles->processInbound($path, $request)
    );
  }

  /**
   * Returns a static export files path on correct path.
   */
  public function testReturnsStaticExportFilesPathOnCorrectPath(): void {
    $path = '/static/export/files/valid-url-target';
    $expectedPath = '/static/export/files';
    $request = Request::create($path);

    $this->assertEquals(
      $expectedPath,
      $this->pathProcessorFiles->processInbound($path, $request)
    );

    $this->assertEquals(
      'valid-url-target',
      $request->query->get('uri_target')
    );
  }

}
