<?php

namespace Drupal\static_suite\Cli;

use Drupal\static_suite\Cli\Process\CliCommandRunningProcessFactoryInterface;
use Drupal\static_suite\Cli\Result\CliCommandResultFactoryInterface;

/**
 * A factory that creates CliCommandInterface objects.
 */
class CliCommandFactory implements CliCommandFactoryInterface {

  /**
   * The CLI command result factory.
   *
   * @var \Drupal\static_suite\Cli\Result\CliCommandResultFactoryInterface
   */
  protected CliCommandResultFactoryInterface $cliCommandResultFactory;

  /**
   * The CLI command running process factory.
   *
   * @var \Drupal\static_suite\Cli\Process\CliCommandRunningProcessFactoryInterface
   */
  protected CliCommandRunningProcessFactoryInterface $cliCommandRunningProcessFactory;

  /**
   * Creates a new instance of CliCommandFactory.
   *
   * @param \Drupal\static_suite\Cli\Result\CliCommandResultFactoryInterface $cliCommandResultFactory
   *   The CLI command result factory.
   * @param \Drupal\static_suite\Cli\Process\CliCommandRunningProcessFactoryInterface $cliCommandRunningProcessFactory
   *   The CLI command running process factory.
   */
  public function __construct(CliCommandResultFactoryInterface $cliCommandResultFactory, CliCommandRunningProcessFactoryInterface $cliCommandRunningProcessFactory) {
    $this->cliCommandResultFactory = $cliCommandResultFactory;
    $this->cliCommandRunningProcessFactory = $cliCommandRunningProcessFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function create(string $cmd, ?string $cwd = NULL, ?array $env = NULL): CliCommandInterface {
    return new CliCommand($this->cliCommandResultFactory, $this->cliCommandRunningProcessFactory, $cmd, $cwd, $env);
  }

}
