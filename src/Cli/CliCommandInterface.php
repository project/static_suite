<?php

namespace Drupal\static_suite\Cli;

use Drupal\static_suite\Cli\Process\CliCommandRunningProcessInterface;
use Drupal\static_suite\Cli\Result\CliCommandResultInterface;

/**
 * An interface for CLI commands.
 */
interface CliCommandInterface {

  /**
   * Open a new CLI process.
   *
   * It should be closed using close() once the process is finished.
   *
   * @return \Drupal\static_suite\Cli\Process\CliCommandRunningProcessInterface
   *   A running process from a CLI command call.
   *
   * @throws \Drupal\static_suite\StaticSuiteException
   *   If the command cannot be opened.
   */
  public function open(): CliCommandRunningProcessInterface;

  /**
   * Close a previously open CLI process.
   *
   * @return int
   *   The exit code (termination status) of the process that was run.
   */
  public function close(): int;

  /**
   * Executes the command.
   *
   * @param string|null $stdIn
   *   Content that will be piped to the command.
   *
   * @return \Drupal\static_suite\Cli\Result\CliCommandResultInterface
   *   An object instance of CliCommandResultInterface.
   *
   * @throws \Drupal\static_suite\StaticSuiteException
   *   If the command cannot be executed.
   */
  public function execute(?string $stdIn = NULL): CliCommandResultInterface;

}
