<?php

namespace Drupal\static_suite\Cli\Process;

/**
 * A running process from a CLI command call.
 *
 * Provides access to process, stdout and stderr stream resources.
 */
class CliCommandRunningProcess implements CliCommandRunningProcessInterface {

  /**
   * A resource representing the process.
   *
   * @var resource
   */
  protected $process;

  /**
   * The stdin stream resource.
   *
   * @var resource
   */
  protected $stdIn;

  /**
   * The stdout stream resource.
   *
   * @var resource
   */
  protected $stdOut;

  /**
   * The stderr stream resource.
   *
   * @var resource
   */
  protected $stdErr;

  /**
   * Creates a running process from a CLI command call.
   *
   * @param resource $process
   *   The resource representing the process.
   * @param resource $stdIn
   *   The stdin stream resource.
   * @param resource $stdOut
   *   The stdout stream resource.
   * @param resource $stdErr
   *   The stderr stream resource.
   */
  public function __construct($process, $stdIn, $stdOut, $stdErr) {
    $this->process = $process;
    $this->stdIn = $stdIn;
    $this->stdOut = $stdOut;
    $this->stdErr = $stdErr;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcess() {
    return $this->process;
  }

  /**
   * {@inheritdoc}
   */
  public function writeStdIn(string $input): int | bool {
    return fwrite($this->stdIn, $input);
  }

  /**
   * {@inheritdoc}
   */
  public function readStdOut(): string | bool {
    return fgets($this->stdOut);
  }

  /**
   * {@inheritdoc}
   */
  public function readStdErr(): string | bool {
    return fgets($this->stdErr);
  }

  /**
   * {@inheritdoc}
   */
  public function getStdOutContents(): ?string {
    if (is_resource($this->stdOut)) {
      return stream_get_contents($this->stdOut);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getStdErrContents(): ?string {
    if (is_resource($this->stdErr)) {
      return stream_get_contents($this->stdErr);
    }
    return NULL;
  }

}
