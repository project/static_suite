<?php

namespace Drupal\static_suite\Cli\Process;

/**
 * A factory that creates RunningCliCommandResultInterface objects.
 */
class CliCommandRunningProcessFactory implements CliCommandRunningProcessFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function create($process, $stdIn, $stdOut, $stdErr): CliCommandRunningProcessInterface {
    return new CliCommandRunningProcess($process, $stdIn, $stdOut, $stdErr);
  }

}
