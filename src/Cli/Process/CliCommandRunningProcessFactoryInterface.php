<?php

namespace Drupal\static_suite\Cli\Process;

/**
 * An interface for factories that create CliCommandRunningProcessInterface.
 */
interface CliCommandRunningProcessFactoryInterface {

  /**
   * Creates a RunningCliCommandResultInterface object.
   *
   * @param resource $process
   *   The resource representing the process.
   * @param resource $stdIn
   *   The stdin stream resource from the running command.
   * @param resource $stdOut
   *   The stdout stream resource from the running command.
   * @param resource $stdErr
   *   The stderr stream resource from the running command.
   *
   * @return CliCommandRunningProcessInterface
   *   A running CLI command result.
   */
  public function create($process, $stdIn, $stdOut, $stdErr): CliCommandRunningProcessInterface;

}
