<?php

namespace Drupal\static_suite\Cli\Process;

/**
 * An interface to define a running process from a CLI command call.
 *
 * Provides access to process, stdout and stderr stream resources.
 */
interface CliCommandRunningProcessInterface {

  /**
   * Get the resource representing the process.
   *
   * @return resource
   *   The resource representing the process, as returned by proc_open()
   */
  public function getProcess();

  /**
   * Write into stdin stream resource.
   *
   * @param string $input
   *   The input to write into stdin.
   *
   * @return int|false
   *   The number of bytes written, or false on error.
   */
  public function writeStdIn(string $input): int | bool;

  /**
   * Sequentially read stdout stream resource from the running command.
   *
   * @return string|false
   *   A string or false if end of stream reached.
   */
  public function readStdOut(): string | bool;

  /**
   * Sequentially read stderr stream resource from the running command.
   *
   * @return string|false
   *   A string or false if end of stream reached.
   */
  public function readStdErr(): string | bool;

  /**
   * Get contents as a string from stdout stream resource.
   *
   * @return string|null
   *   The contents from stdout stream resource
   */
  public function getStdOutContents(): ?string;

  /**
   * Get contents as a string from stderr stream resource.
   *
   * @return string|null
   *   The contents from stderr stream resource
   */
  public function getStdErrContents(): ?string;

}
