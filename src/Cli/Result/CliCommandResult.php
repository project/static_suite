<?php

namespace Drupal\static_suite\Cli\Result;

/**
 * The result of a CLI call.
 *
 * Provides access to stdout/stderr contents and the return code.
 */
class CliCommandResult implements CliCommandResultInterface {

  /**
   * The return code.
   *
   * @var int
   */
  protected int $exitCode;

  /**
   * The stdout contents.
   *
   * @var string|null
   */
  protected ?string $stdOutContents;

  /**
   * The stderr contents.
   *
   * @var string|null
   */
  protected ?string $stdErrContents;

  /**
   * Creates a new result for a CLI command.
   *
   * @param int $exitCode
   *   The return code.
   * @param string|null $stdOutContents
   *   The stdout contents.
   * @param string|null $stdErrContents
   *   The stderr contents.
   */
  public function __construct(int $exitCode, ?string $stdOutContents = NULL, ?string $stdErrContents = NULL) {
    $this->exitCode = $exitCode;
    $this->stdOutContents = $stdOutContents;
    $this->stdErrContents = $stdErrContents;
  }

  /**
   * {@inheritdoc}
   */
  public function getExitCode(): int {
    return $this->exitCode;
  }

  /**
   * {@inheritdoc}
   */
  public function getStdOutContents(): ?string {
    return $this->stdOutContents;
  }

  /**
   * {@inheritdoc}
   */
  public function getStdErrContents(): ?string {
    return $this->stdErrContents;
  }

}
