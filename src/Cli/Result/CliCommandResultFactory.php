<?php

namespace Drupal\static_suite\Cli\Result;

/**
 * A factory that creates CliCommandResultInterface objects.
 */
class CliCommandResultFactory implements CliCommandResultFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function create(int $exitCode, ?string $stdOutContents = NULL, ?string $stdErrContents = NULL): CliCommandResultInterface {
    return new CliCommandResult($exitCode, $stdOutContents, $stdErrContents);
  }

}
