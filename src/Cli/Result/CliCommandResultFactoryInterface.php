<?php

namespace Drupal\static_suite\Cli\Result;

/**
 * An interface for factories that create CliCommandResultInterface objects.
 */
interface CliCommandResultFactoryInterface {

  /**
   * Creates a CliCommandResultInterface object.
   *
   * @param int $exitCode
   *   The return code.
   * @param string|null $stdOutContents
   *   The stdout contents.
   * @param string|null $stdErrContents
   *   The stderr contents.
   *
   * @return CliCommandResultInterface
   *   A CLI command result.
   */
  public function create(int $exitCode, ?string $stdOutContents = NULL, ?string $stdErrContents = NULL): CliCommandResultInterface;

}
