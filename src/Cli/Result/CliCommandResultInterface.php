<?php

namespace Drupal\static_suite\Cli\Result;

/**
 * An interface to define the result of a CLI call.
 *
 * Provides access to stdout, stderr and the return code.
 */
interface CliCommandResultInterface {

  /**
   * Returns the contents of stdout.
   *
   * @return string|null
   *   The contents of stdout.
   */
  public function getStdOutContents(): ?string;

  /**
   * Returns the contents of stderr.
   *
   * @return string|null
   *   The contents of stderr.
   */
  public function getStdErrContents(): ?string;

  /**
   * Returns the return code.
   *
   * @return int
   *   The return code.
   */
  public function getExitCode(): int;

}
