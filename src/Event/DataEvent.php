<?php

namespace Drupal\static_suite\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event for altering or reading generic data.
 */
class DataEvent extends Event {

  /**
   * An array with event's data.
   *
   * @var array
   */
  protected array $data = [];

  /**
   * Constructs the object.
   *
   * @param array $data
   *   Event data.
   */
  public function __construct(array $data) {
    $this->setData($data);
  }

  /**
   * Get event data.
   *
   * @return array
   *   The event data.
   */
  public function getData(): array {
    return $this->data;
  }

  /**
   * Gets an item from event data.
   *
   * @param string $key
   *   The key of the data item.
   *
   * @return mixed
   *   The data item, or NULL if key is not found.
   */
  public function getDataItem(string $key): mixed {
    return $this->data[$key] ?? NULL;
  }

  /**
   * Set event data.
   *
   * @param array $data
   *   Event data.
   */
  public function setData(array $data): void {
    $this->data = $data;
  }

  /**
   * Sets an item event data.
   *
   * @param string $key
   *   The key of the data item.
   * @param mixed $data
   *   The new data item.
   */
  public function setDataItem(string $key, mixed $data): void {
    $this->data[$key] = $data;
  }

}
