<?php

namespace Drupal\static_suite;

/**
 * Indicates a problem with Static Suite and/or its modules.
 */
class StaticSuiteException extends \Exception {

}
