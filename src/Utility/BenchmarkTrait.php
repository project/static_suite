<?php

namespace Drupal\static_suite\Utility;

/**
 * Methods for measuring the performance of a process.
 */
trait BenchmarkTrait {

  /**
   * Benchmark start time.
   *
   * @var float|null
   */
  protected ?float $timeStart;

  /**
   * Benchmark end time.
   *
   * @var float|null
   */
  protected ?float $timeEnd;

  /**
   * Starts benchmark.
   */
  protected function startBenchmark(): void {
    $this->timeStart = microtime(TRUE);
    $this->timeEnd = NULL;
  }

  /**
   * Ends benchmark.
   */
  protected function endBenchmark(): void {
    $this->timeEnd = microtime(TRUE);
  }

  /**
   * Gets elapsed time.
   *
   * @return float
   *   Elapsed time.
   */
  protected function getBenchmark(): float {
    $timeEnd = $this->timeEnd ?: microtime(TRUE);
    return $timeEnd - $this->timeStart;
  }

  /**
   * Formats elapsed time as a string.
   *
   * @param int $precision
   *   The optional number of decimal digits to round to. Defaults to 3.
   * @param int $decimals
   *   Optional number of decimal points. Defaults to 3.
   *
   * @return string
   *   Formatted elapsed time.
   */
  protected function formatBenchmark(int $precision = 3, int $decimals = 3): string {
    return number_format(round($this->getBenchmark(), $precision), $decimals);
  }

}
