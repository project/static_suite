<?php

namespace Drupal\Tests\static_suite\Unit;

use Drupal\static_suite\Event\DataEvent;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\static_suite\Event\DataEvent
 *
 * @group static_suite
 */
class DataEventTest extends UnitTestCase {

  protected const TEST_DATA = [
    'irrelevantKey' => 'irrelevantValue',
  ];

  /**
   * Static suite data event.
   *
   * @var \Drupal\static_suite\Event\DataEvent
   */
  protected DataEvent $dataEvent;

  /**
   * Initialization of the dependencies.
   */
  protected function setUp(): void {
    parent::setUp();
    $this->dataEvent = new DataEvent(self::TEST_DATA);
  }

  /**
   * @covers ::getData
   */
  public function testGetEventData(): void {
    $expectedData = self::TEST_DATA;

    $this->assertEquals(
      $expectedData,
      $this->dataEvent->getData()
    );
  }

  /**
   * @covers ::getDataItem
   */
  public function testGetEventDataItem(): void {
    $expectedData = 'irrelevantValue';

    $this->assertEquals(
      $expectedData,
      $this->dataEvent->getDataItem('irrelevantKey')
    );
  }

  /**
   * @covers ::setData
   */
  public function testSetData(): void {
    $newData = [
      'irrelevantNewDataKey' => 'irrelevantNewDataValue',
    ];
    $expectedData = $newData;

    $this->dataEvent->setData($newData);

    $this->assertEquals(
      $expectedData,
      $this->dataEvent->getData()
    );
  }

  /**
   * @covers ::setDataItem
   */
  public function testSetDataItem(): void {
    $newData = 'newIrrelevantValue';
    $expectedData = $newData;

    $this->dataEvent->setDataItem('irrelevantKey', $newData);

    $this->assertEquals(
      $expectedData,
      $this->dataEvent->getDataItem('irrelevantKey')
    );
  }

}
