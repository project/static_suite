<?php

namespace Drupal\Tests\static_suite\Unit;

use Drupal\static_suite\Utility\StaticSuiteUtils;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\static_suite\Utility\StaticSuiteUtils
 *
 * @group static_suite
 */
class StaticSuiteUtilsTest extends UnitTestCase {

  /**
   * Static suite utils service.
   *
   * @var \Drupal\static_suite\Utility\StaticSuiteUtils
   */
  protected StaticSuiteUtils $staticSuiteUtils;

  /**
   * Initialization of the dependencies.
   */
  protected function setUp(): void {
    parent::setUp();
    $this->staticSuiteUtils = new StaticSuiteUtils();
  }

  /**
   * @covers ::getFormattedMicroDate
   *
   * @dataProvider providerElementsToGetFormattedMicroDate
   */
  public function testGetFormattedMicroDate($format, $time, $expected): void {
    $this->assertEquals(
      $expected,
      $this->staticSuiteUtils->getFormattedMicroDate($format, $time)
    );
  }

  /**
   * Provides a list of elements to test for testGetFormattedMicroDate().
   *
   * @return array
   *   The data for each test scenario.
   */
  public static function providerElementsToGetFormattedMicroDate(): array {
    $timestamp = '43200';
    return [
      [
        'Y-m-d',
        $timestamp,
        '1970-01-01',
      ],
      [
        'd-m-Y',
        $timestamp,
        '01-01-1970',
      ],
      [
        'Y-m-d_H-i-s.u',
        $timestamp,
        '1970-01-01_12-00-00.000000',
      ],
    ];
  }

  /**
   * @covers ::isAnyItemMatchingRegexpList
   *
   * @dataProvider providerElementsToIsAnyItemMatchingRegexpList
   */
  public function testIsAnyItemMatchingRegexpList($items, $regex, $expected): void {
    $this->assertEquals(
      $expected,
      $this->staticSuiteUtils->isAnyItemMatchingRegexpList($items, $regex)
    );
  }

  /**
   * Provides a list of elements to test for testIsAnyItemMatchingRegexpList().
   *
   * @return array
   *   The data for each test scenario.
   */
  public static function providerElementsToIsAnyItemMatchingRegexpList(): array {
    return [
      [
        ['value1', 'value2', 'value2'],
        ['val'],
        TRUE,
      ],
      [
        ['', 'test', 'value'],
        ['val'],
        TRUE,
      ],
      [
        ['value1', 'value2', 'value2'],
        ['not-found'],
        FALSE,
      ],
      [
        [000, 12, 'value2'],
        ['[6-7]', '[4-5]', '[7-9]'],
        FALSE,
      ],
      [
        [345, 12, 'value2'],
        ['[6-7]', '[4-5]', '[7-9]'],
        TRUE,
      ],
    ];
  }

  /**
   * @covers ::removeDotSegments
   *
   * @dataProvider providerElementsToRemoveDotSegments
   */
  public function testRemoveDotSegments($data, $expected): void {
    $this->assertEquals(
      $expected,
      $this->staticSuiteUtils->removeDotSegments($data)
    );
  }

  /**
   * Provides a list of elements to test for testRemoveDotSegments().
   *
   * @return array
   *   The data for each test scenario.
   */
  public static function providerElementsToRemoveDotSegments(): array {
    return [
      [
        '../path',
        'path',
      ],
      [
        '/test/../path',
        '/path',
      ],
      [
        '/../../path',
        '/path',
      ],
      [
        '/path/..',
        '/',
      ],
      [
        '/one/../second/../third',
        '/third',
      ],
      [
        '/one/../second/../third/',
        '/third/',
      ],
    ];
  }

}
