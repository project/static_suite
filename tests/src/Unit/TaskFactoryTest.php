<?php

namespace Drupal\Tests\static_suite\Unit;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\File\FileSystemInterface;
use Drupal\static_suite\Release\Task\TaskFactory;
use Drupal\Tests\UnitTestCase;

/**
 * Tests for task factory.
 *
 * @coversDefaultClass \Drupal\static_suite\Release\Task\TaskFactory
 *
 * @group static_suite
 */
class TaskFactoryTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * @covers ::create
   *
   * Checks if the log file is under the expected location.
   */
  public function testReturnsNewInstanceOfTaskService(): void {
    $taskFactory = new TaskFactory();
    $fileSystem = $this->prophesize(FileSystemInterface::class);
    $expected = 'dir/id/id.log';

    $taskInstance = $taskFactory->create($fileSystem->reveal(), 'dir', 'id');

    $this->assertEquals(
      $expected,
      $taskInstance->getLogFilePath()
    );
  }

}
