<?php

namespace Drupal\Tests\static_suite\Unit;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\File\FileSystemInterface;
use Drupal\static_suite\Release\Task\Task;
use Drupal\static_suite\Release\Task\TaskInterface;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use RecursiveIteratorIterator;
use Symfony\Component\Finder\Iterator\RecursiveDirectoryIterator;

/**
 * Tests for Task.
 *
 * @coversDefaultClass \Drupal\static_suite\Release\Task\Task
 *
 * @group static_suite
 */
class TaskTest extends UnitTestCase {

  use ProphecyTrait;
  /**
   * Task service.
   *
   * @var \Drupal\static_suite\Release\Task\TaskInterface
   */
  protected TaskInterface $taskService;

  protected const DIR = __DIR__ . '/temp-data';

  protected const ID = 'id';

  /**
   * Initialization of the dependencies.
   */
  protected function setUp(): void {
    parent::setUp();
    $fileSystem = $this->prophesize(FileSystemInterface::class);
    $fileSystem->dirname(Argument::any())
      ->will(function ($file) {
        return dirname($file[0]);
      });
    $fileSystem->unlink(Argument::any())
      ->will(function ($file) {
        if (is_file($file[0])) {
          unlink($file[0]);
        }
        return TRUE;
      });

    $this->taskService = new Task($fileSystem->reveal(), self::DIR, self::ID);
    $this->createFlagsTempFolder();
  }

  /**
   * Clean flags temp folder.
   */
  protected function tearDown(): void {
    parent::tearDown();
    $dir = self::DIR;
    $dir = new RecursiveDirectoryIterator($dir, \FilesystemIterator::SKIP_DOTS);
    $dir = new RecursiveIteratorIterator($dir, RecursiveIteratorIterator::CHILD_FIRST);
    foreach ($dir as $file) {
      $file->isDir() ? rmdir($file) : unlink($file);
    }

    rmdir(self::DIR);
  }

  /**
   * @covers ::isFlagSet
   *
   * @throws \Drupal\static_suite\StaticSuiteException
   */
  public function testIsFlagSet(): void {
    $flagName = 'irrelevantFlag';

    $this->taskService->setFlag($flagName, TRUE);

    $this->assertTrue(
      $this->taskService->isFlagSet($flagName)
    );
  }

  /**
   * @covers ::isStarted
   *
   * @throws \Drupal\static_suite\StaticSuiteException
   */
  public function testIsStarted(): void {
    $this->taskService->setFlag('id-' . TaskInterface::STARTED, TRUE);

    $this->assertTrue(
      $this->taskService->isStarted()
    );

    $this->taskService->setFlag('id-' . TaskInterface::STARTED, FALSE);

    $this->assertFalse(
      $this->taskService->isStarted()
    );
  }

  /**
   * @covers ::isFailed
   *
   * @throws \Drupal\static_suite\StaticSuiteException
   */
  public function testIsFailed(): void {
    $this->taskService->setFailed();

    $this->assertTrue(
      $this->taskService->isFailed()
    );
  }

  /**
   * If the process not done, the log file must have been recently modified.
   *
   * @covers ::isFailed
   */
  public function testIsFailedWhenTheProcessItsNotDone(): void {
    $this->createLogFile();

    $this->assertFalse(
      $this->taskService->isFailed()
    );
  }

  /**
   * If the process done, it's not failed.
   *
   * @covers ::isFailed
   *
   * @throws \Drupal\static_suite\StaticSuiteException
   */
  public function testIsFailedWhenTheProcessHasDone(): void {
    $this->taskService->setDone();

    $this->assertFalse(
      $this->taskService->isFailed()
    );
  }

  /**
   * @covers ::isDone
   *
   * @throws \Drupal\static_suite\StaticSuiteException
   */
  public function testIsDone(): void {
    $this->taskService->setDone();

    $this->assertTrue(
      $this->taskService->isDone()
    );

  }

  /**
   * @covers ::isRunning
   *
   * @throws \Drupal\static_suite\StaticSuiteException
   */
  public function testIsRunning(): void {
    $this->taskService->setStarted();
    $this->createLogFile();

    $this->assertTrue(
      $this->taskService->isRunning()
    );
  }

  /**
   * @covers ::getLogFilePath
   */
  public function testGetLogFilePath(): void {
    $expected = self::DIR . '/' . self::ID . '/' . self::ID . '.log';

    $this->assertEquals(
      $expected,
      $this->taskService->getLogFilePath()
    );
  }

  /**
   * Creates flags temp folder.
   */
  private function createFlagsTempFolder(): void {
    $folder = self::DIR . '/' . self::ID;
    if (!is_dir($folder)) {
      mkdir($folder, 0777, TRUE);
    }
  }

  /**
   * Creates log file to emulate is not done and not failed.
   */
  private function createLogFile(): void {
    file_put_contents($this->taskService->getLogFilePath(), 'irrelevant');
  }

}
