<?php

namespace Drupal\Tests\static_suite\Unit;

use Drupal\static_suite\StaticSuiteException;
use Drupal\static_suite\Utility\StaticSuiteUtils;
use Drupal\static_suite\Utility\UniqueIdHelper;
use Drupal\Tests\UnitTestCase;

/**
 * Tests for unique id helper service.
 *
 * @coversDefaultClass \Drupal\static_suite\Utility\UniqueIdHelper
 *
 * @group static_suite
 */
class UniqueIdHelperTest extends UnitTestCase {

  /**
   * Unique id helper service.
   *
   * @var \Drupal\static_suite\Utility\UniqueIdHelper
   */
  protected UniqueIdHelper $uniqueIdHelper;

  /**
   * Initialization of the dependencies.
   */
  protected function setUp(): void {
    parent::setUp();
    $staticSuiteUtilsService = new StaticSuiteUtils();
    $this->uniqueIdHelper = new UniqueIdHelper($staticSuiteUtilsService);
  }

  /**
   * @covers ::getUniqueId
   */
  public function testGetsNewUniqueId(): void {
    $uniqueId = $this->uniqueIdHelper->getUniqueId();
    $this->assertTrue(
      $this->uniqueIdHelper->isUniqueId($uniqueId)
    );
  }

  /**
   * @covers ::generateUniqueId
   */
  public function testGenerateUniqueId(): void {
    $uniqueId = $this->uniqueIdHelper->generateUniqueId();
    $this->assertTrue(
      $this->uniqueIdHelper->isUniqueId($uniqueId)
    );
  }

  /**
   * @covers ::getDefaultUniqueId
   */
  public function testReturnsDefaultId(): void {
    $expected = "1970-01-01_12-00-00.000000__0000";
    $defaultId = $this->uniqueIdHelper->getDefaultUniqueId();
    $this->assertEquals(
      $expected,
      $defaultId
    );

    $this->assertTrue(
      $this->uniqueIdHelper->isUniqueId($defaultId)
    );
  }

  /**
   * @covers ::isUniqueId
   */
  public function testCheckIfUniqueIdHasCorrectFormat(): void {
    $this->assertFalse($this->uniqueIdHelper->isUniqueId('invalidId'));
    $this->assertFalse($this->uniqueIdHelper->isUniqueId('1970-01-01'));
    $this->assertFalse($this->uniqueIdHelper->isUniqueId('9999-99-99_12-00-00.000000__0000'));
    $this->assertTrue($this->uniqueIdHelper->isUniqueId('1970-01-01_12-00-00.000000__0000'));
  }

  /**
   * @covers ::getDateFromUniqueId
   *
   * @throws \Drupal\static_suite\StaticSuiteException
   */
  public function testReturnsExpectedDatetimeFromUniqueId(): void {
    $expectedTimestamp = '43200';
    $uniqueId = '1970-01-01_12-00-00.000000__0000';
    $date = $this->uniqueIdHelper->getDateFromUniqueId($uniqueId);
    $this->assertEquals(
      $expectedTimestamp,
      $date->getTimestamp()
    );
  }

  /**
   * @covers ::getDateFromUniqueId
   *
   * @throws \Drupal\static_suite\StaticSuiteException
   */
  public function testThrowsExceptionIfTryToRetrieveDateFromInvalidId(): void {
    $uniqueId = 'invalid-id';

    $this->expectException(StaticSuiteException::class);

    $this->uniqueIdHelper->getDateFromUniqueId($uniqueId);
  }

}
